---
module-name: "GitLab Installation and Administration Basics"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - TBD
---

## Introduction

Welcome to the next step in your onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

### Goals of this checklist

At the end of the checklist, new team member should
- Be comfortable with the different installation options of GitLab.
- Have an installation available for reproducing customer bug reports.
- Get your development machine set up.

### General Timeline and Expectations*

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **one week to complete**.

## Installation and Administration

As a Support Engineer you will keep one personal installation of GitLab continually updated on Google Cloud Platform (managed through Terraform), just like many of our clients. This installation will serve as your goto spot to quickly reproduce customer bug reports. Additionally, you should make yourself familiar with other ways to install GitLab.
**Note**: When setting up older versions of GitLab, it's important to keep in mind that they may pose a risk due to unpatched security vulnerabilities. If you're testing an older version of GitLab and the instance is internet facing, it is strongly recommended to apply [IP filtering](https://handbook.gitlab.com/handbook/support/workflows/test_env/#securing-cloud-testing-environments) and vulnerability mitigations to minimize the risk of exploitation.  

1. [ ] Create a new test instance after [reading up on the different kinds of test environments](https://handbook.gitlab.com/handbook/support/workflows/test_env/).
    1. If you have any issues or questions, ask in the [`#support_testing` channel](https://gitlab.slack.com/archives/C0167JB9E02).
1. [ ] [Read up about our different Installation Methods](https://about.gitlab.com/installation/)
    1. [ ] Set up your test environments; make sure to set up at least:
        1. [ ] GCP (for example using [Sandbox Cloud using Terraform](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-create-a-terraform-environment))
        1. [ ] A local VM
            - **Note**: Docker Desktop [requires a license](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop), please consider other methods. If you're using a Mac with M1 / Apple Silicon, running GitLab with Docker might not work yet. In this case, try using a [local VM via UTM](https://handbook.gitlab.com/handbook/support/workflows/test_env/#utm-free--opensource---compatible-with-apple-silicon) as an alternative that's confirmed to work.
        1. [ ] Feel free to check out alternative environments like AWS, Azure or different approaches for local VMs like UTM, VMWare, Vagrant, etc.
1. [ ] Install GitLab in the test environments you set up above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) on GCP
       1. [ ] Populate with some test data: User account, Project, Issue
           - [Utilize the GitLab API to seed your test instance](https://gitlab.com/gitlab-com/support/support-training/blob/main/seed-data-api.md)
       1. [ ] Backup using our [Backup rake task](https://docs.gitlab.com/ee/administration/backup_restore/backup_gitlab.html)
   1. [ ] Install via Docker or a local VM on your local machine.
       1. [ ] Restore the backup you just created to this installation using our [Restore rake task](https://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-gitlab)
   1. [ ] Install a [GitLab Runner](https://docs.gitlab.com/runner/) and connect it to your GitLab instance.
1. [ ] Add [Elastic Search](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html) to your test environments:
    1. [ ] Install on a VM or a separate instance on Terraform
        * The [steps outlined in our support resources documentation](https://gitlab.com/gitlab-com/support/support-resources/-/blob/master/README.md) should help you configure your instance's IP.
        * Set `cluster.name` and `node.name` and use the latter in `cluster.initial_master_nodes` to enable ["single node" mode](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/add-elasticsearch-nodes.html).
    1. [ ] [Connect Elastic Search to your GitLab instance and index your repositories](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html). You need to apply a license to your GitLab instance for the integration settings to appear if you haven't already.
1. [ ] Read up about the support workflow for [Generating GitLab Team Member Licenses](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/policies/team_member_licenses/)
   1. [ ] Use the [GitLab Team Member License request template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=GitLab_Team_Member_License_request)  to request for licenses for Self-Managed Premium and Ultimate tiers. **Ensure that you create a separate issue for each type of license.** [Refer to the pricing information page to compare feature differences between each tier](https://about.gitlab.com/pricing/).
   1. [ ] Store your licenses securely in 1Password. Use the licenses on your test instance when replicating customer issues. By having the correct license, you will ensure feature parity with what the customer is seeing.
1. [ ] Watch the [2022 GitLab Debugging Techniques: A Support Engineering Perspective](https://www.youtube.com/watch?v=bU00b-4pM9Q&t) video on GitLab Unfiltered (YouTube). This dives into some common issues that customers might experience.
1. [ ] Keep this installation up-to-date as patch and version releases become available, just like our customers would. If your test instance is publicly available, keep it secured and disable user Sign-ups.
       You can check this box, but please keep this installation up-to-date as long as you are a Support Engineer for GitLab.

### (Optional) Setting up some services to use with GitLab

The services below are part of the many services used by our customers. Installing these services and learning how they work with GitLab will give you a solid foundation for when you're ready to resolve tickets on your own. Complete as many of these now as you are interested in, but be sure to familiarize yourself with all of them in the future. Consider pairing with your onboarding buddy to learn from any challenges which may arise!

1. [ ] Connect your GitLab instance to a cluster via the [Kubernetes clusters](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html) integration and install a Runner in the cluster:
    1. [ ] Log in to https://gitlabsandbox.cloud/ and sign in with your Okta account to get access to the [GitLab Sandbox Cloud](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/). If you haven't created your account yet, this is a good opportunity to do so by following these [steps](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started).
    1. [ ] Once you have created an account, you can proceed and create a cluster under your own GCP project. Start small to [cut costs to about one tenth](https://about.gitlab.com/handbook/values/#spend-company-money-like-its-your-own) of the default cluster config:
            - Set `Location type` to a single zone (instead of a region)
            - Set `Node pool` `Size` to 1
            - Set the `Nodes`' `Machine type` to a `smaller` one than the default (recommend `e2-medium` for 2vCPU and 4GB RAM)
            - Review the [general tips for cost optimization](https://handbook.gitlab.com/handbook/support/workflows/cost-optimizations-with-cloud-instances/#general-tips-for-cost-optimization)
    1. [ ] Find your cluster and click `connect` next to your cluster. Then click on `Run in Cloud Shell`.
    1. [ ] [Add](https://docs.gitlab.com/ee/user/clusters/agent/) the Kubernetes cluster to GitLab. Use the [Cloud Shell](https://cloud.google.com/shell/docs/how-cloud-shell-works) to run the `kubectl` commands as needed. ** Needs updating **
        - Be aware that when copying data from the Cloud Shell, it might add extra characters to soft wrapped strings.
    1. [ ] Create a project on your self-managed instance and then create a file named `.gitlab-ci.yml`.
    1. [ ] Use [the YAML docs](https://docs.gitlab.com/ee/ci/yaml/#parameter-details) as a resource to build a basic script.
    1. [ ] Make sure your project has access to your Kubernetes cluster under Operations -> Kubernetes.
    1. [ ] Make sure your project has registered runners under your project's Settings -> CI/CD -> Runners.
    1. [ ] Pat yourself on the back; you're now ready to reproduce some of our common customer tickets.
    1. [ ] Remove the cluster when you're done!
1. [ ] Install GitLab from [Source](https://docs.gitlab.com/ee/install/installation.html).
    Installation from source is not common but will give you a greater understanding
    of the components that we employ and how everything fits together.
        - [Have a copy of GitLab source code available offline for reference](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1900)
1. [ ] In the future, you may need other applications for testing. Don't set these up now, but check out the [infrastructure for troubleshooting section](https://handbook.gitlab.com/handbook/support/workflows/#infrastructure-for-troubleshooting) of workflows, so you know what's available.

## (Optional) Setting up GDK

1. [ ] You can install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/), which is what you'd need later on in case you want to contribute code changes to GitLab.
    - If you like Docker, especially if you're running Linux on your desktop, consider the [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit/) as an alternative.

## Congratulations! You made it, and now have an understanding of the different ways in which GitLab can be installed and managed.

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://handbook.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::GitLab Installation and Administration Basics"
/assign me
