---
module-name: "Maven"
area: "Product Knowledge"
gitlab-group: "Package:Package"
maintainers:
  - TBD
---

## Overview

**Goal**: Set a clear path for training on Maven and the features GitLab offers that interact with Maven

**Objectives**: At the end of this module, you should be able to:
- Know how Maven works
- Know how Maven interoperates with GitLab's features

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: <module title> - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical <module title> questions to you.
1. [ ] In your Slack Notification Settings, set **Maven** and **Java** as
      [Keyword Notifications](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications).
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Technical setup

- [ ] **Done with Stage 1**

  1. [ ] Install OpenJDK on your machine: https://pandacodez.com/tutorial/install-latest-jdk-java-on-mac-using-homebrew/
  1. [ ] Install Maven: https://www.code2bits.com/how-to-install-maven-on-macos-using-homebrew/
  1. [ ] Create a Hello World Spring Boot application, it will be useful for testing: https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started-first-application

## Stage 2: Become familiar with Maven

- [ ] **Done with Stage 2**

1. [ ] The Maven documentation is a bit erratic. It mixes basic explanations with more esoteric ones. If you think things are getting too abstract or advanced, feel free to skip it.
1. [ ] Read and go through the examples in https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html
1. [ ] Read about the build lifecycle https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
1. [ ] Read about the POM file: https://maven.apache.org/guides/introduction/introduction-to-the-pom.html
1. [ ] Read about the dependency mechanism: https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html
1. [ ] Skim https://maven.apache.org/guides/getting-started/index.html, read the sections that draw your attention
1. [ ] Skim at least the beginning of https://maven.apache.org/settings.html and https://maven.apache.org/pom.html

1. [ ] **Optional** Configure Maven to operate through an HTTP proxy

## Stage 3: Integrate your test project with GitLab features

- [ ] **Done with Stage 3**

1. [ ] Configure a CI cache for Maven, so that it won't download all dependencies on every single build
  1. [ ] You can apply the Maven template: https://gitlab.com/gitlab-org/gitlab/blob/master/lib%2Fgitlab%2Fci%2Ftemplates%2FMaven.gitlab-ci.yml
1. [ ] Run Auto Devops with your test project
1. [ ] Configure your project to deploy to the GitLab Maven repository
1. [ ] Configure a CI job that will deploy to the Maven repository when changes are merged into master.
1. [ ] Configure another project to download dependencies from the GitLab repository
  1. [ ] You can base yourself on https://gitlab.com/gitlab-examples/maven/simple-maven-dep and https://gitlab.com/gitlab-examples/maven/simple-maven-app
1. [ ] **Optional** Configure Maven to deploy to a GitLab with self-signed certs

## Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Contribute valuable responses on at least 10 tickets involving Maven, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay it to
the customer.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 5: Advanced scenarios

1. [ ] From the tickets above, reproduce two scenario that weren't covered in this module. Describe them below and link to the project with the implementation, if it applies.
   1. [ ] __
   1. [ ] __

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and scenarios. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in Maven on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module
/label ~"Module::Maven"
/assign me