---
module-name: openshift
area: "Core Technologies"
maintainers:
  - jdasmarinas
---

## Overview

**Goal**: Learn about OpenShift and OpenShift Operators.

*Length*: <estimate # of hours, use single number or range>

**Objectives**: At the end of this module, you should be able to:
- Differentiate about Kubernetes and OpenShift
- Understand how OpenShift Operators work
- Familiarize yourself with GitLab Operator
- Familiarize yourself with GitLab Runner Operator

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `OpenShift - <your name>`
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical OpenShift questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Set-up a local or cloud OpenShift Environment

**Preliminary notes:**
1. OpenShift Container Platform [is not yet supported on the M1 architecture](
https://access.redhat.com/documentation/en-us/red_hat_openshift_local/2.5/html/getting_started_guide/installation_gsg). If you are using M1 Mac, consider deploying it to a Linux VM. For instance, you may use a GCP VM.
1. You will need to create a free Red Hat developer account to proceed with downloading and installing OpenShift.

[Follow the steps here to install Red Hat OpenShift Local](https://access.redhat.com/documentation/en-us/red_hat_openshift_local/2.5)
  - Make sure to switch to the latest version of Red Hat OpenShift Local. The latest version at the time of this writing is 2.5.
  - When installation is completed, spin-up a local OpenShift cluster using `crc start`.
  - It should print the password for the `kubeadmin` user and `developer` user.

**Considerations when using a GCP VM:**
1. You will need to [enable virtualization for a VM](https://cloud.google.com/compute/docs/instances/nested-virtualization/enabling). The simplest way to do it is to click `EQUIVALENT COMMAND LINE` instead of creating a VM via UI, add the option `--enable-nested-virtualization` to your command and create a VM via CLI.
1. Make sure that you have at least 9Gb and 4 CPUs in your VM.
1. When using GCP, use `N2` machine type. `E2` and `N2D` are not supported.

## Stage 2: Using OpenShift

Follow these steps first if you are using a GCP VM:
  - add `127.0.0.1    api.crc.testing canary-openshift-ingress-canary.apps-crc.testing console-openshift-console.apps-crc.testing default-route-openshift-image-registry.apps-crc.testing downloads-openshift-console.apps-crc.testing oauth-openshift.apps-crc.testing` to a hosts file on your workstation
  - establish SSH tunnel to your VM via `ssh username@vm-ip-address -i /Users/username/.ssh/id_rsa -L 443:console-openshift-console.apps-crc.testing:443`
  - OpenShift UI should be available via https://console-openshift-console.apps-crc.testing

Explore your new OpenShift cluster:

1. [ ] Login as the `kubeadmin` user using the CLI: `oc login -u kubeadmin https://api.crc.testing:6443`. `oc` supports most of the `kubectl` commands. In addition to that, `oc` also supports OpenShift resources.
1. [ ] Read about [Differences Between oc and kubectl](https://docs.openshift.com/container-platform/3.11/cli_reference/differences_oc_kubectl.html).
1. [ ] Login as the `kubeadmin` user to familiarize yourself with the OpenShift UI: `https://console-openshift-console.apps-crc.testing`
1. [ ] Note that OpenShift uses [Security Context Constraints](https://docs.gitlab.com/operator/security_context_constraints.html). The closest thing in Kubernetes is [Pod Security Policies](https://kubernetes.io/docs/concepts/security/pod-security-policy/) (deprecated) and [Pod Security Admission](https://kubernetes.io/docs/concepts/security/pod-security-admission/). Because of Security Context Constraints, some image might not work properly.
1. [ ] [Deploy an application](https://docs.openshift.com/container-platform/3.11/dev_guide/application_lifecycle/new_app.html) to OpenShift cluster.
1. [ ] Delete the application: `oc delete all,configmap,pvc,serviceaccount,rolebinding --selector app=<APPLICATION_NAME>`.

## Stage 3: Understanding Operators

1. [Understanding the Operator pattern](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/).
1. [Quick introduction to Operators](https://www.redhat.com/en/topics/containers/what-is-a-kubernetes-operator).

## Stage 4: GitLab Runner Operator on OpenShift

You can install the GitLab Runner Operator in OpenShift or Kubernetes. We will focus on OpenShift for this part.

1. List the available CRDs first before installing the GitLab Runner operator.

   ```bash
   oc get crd --all-namespaces > /tmp/crd-before-operator.txt
   ```
   
1. [Install the GitLab Runner Operator using OperatorHub](https://docs.gitlab.com/runner/install/operator.html#install-the-openshift-operator).
  - There is two available GitLab Runner Operator in OperatorHub, **Certified** and **Community**.
  - Pick **Certified**.
1. Check the newly installed operator:

   ```bash
   oc -n openshift-operators get operator

   ## You can also check the deployment itself
   oc -n openshift-operators get deployment
   ```

1. List the available CRDs after install the GitLab Runner operator and compare it with the old list.

   ```bash
   oc get crd --all-namespaces > /tmp/crd-after-operator.txt
   diff /tmp/crd-before-operator.txt /tmp/crd-after-operator.txt
   ```

1. [Install GitLab Runner using the Operator](https://docs.gitlab.com/runner/install/operator.html#install-gitlab-runner).

## Stage 5: GitLab Operator on Kubernetes

At the time of this writing, Red Hat OpenShift Local will deploy an incompatible OpenShift version. To proceed with this, let's install the Operator on Kubernetes.

1. Spin up a Kubernetes cluster using your personal GKE project provisioned via https://gitlabsandbox.cloud
1. [Install Cert Manager](https://cert-manager.io/docs/installation/#default-static-install). It is required  to install GitLab operator: https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/373
1. [Install the GitLab Operator](https://docs.gitlab.com/operator/installation.html#installing-the-gitlab-operator).

   ```bash
   GL_OPERATOR_VERSION=0.10.1 # https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/releases
   PLATFORM=kubernetes
   kubectl create namespace gitlab-system
   kubectl apply -f https://gitlab.com/api/v4/projects/18899486/packages/generic/gitlab-operator/${GL_OPERATOR_VERSION}/gitlab-operator-${PLATFORM}-${GL_OPERATOR_VERSION}.yaml
   ```

1. [Install GitLab using the Operator](https://docs.gitlab.com/operator/installation.html#installing-the-gitlab-operator).

   ```yaml
   apiVersion: apps.gitlab.com/v1beta1
   kind: GitLab
   metadata:
     name: gitlab
     namespace: gitlab-system
   spec:
     chart:
       version: "6.0.4"
       values:
         global:
           hosts:
             domain: insert-your-domain-here
           ingress:
             configureCertmanager: true
         certmanager-issuer:
           email: 'your-email@example.com'
         prometheus:
           install: false
         gitlab-runner:
           install: false
         gitlab:
           webservice:
             enabled: true
           sidekiq:
             enabled: true
   ```

   - This requires that you have a valid domain allocated to the IP address generated by the load balancer.
   - To use a self-signed certificate, you will have to configure this on [cert-manager](https://cert-manager.io/docs/configuration/selfsigned/).
   - At the time of this writing, the operator doesn't support [automatic self-signed certificate generation](https://docs.gitlab.com/charts/charts/globals.html#globalingressconfigurecertmanager).
- Review the doc [Troubleshooting the Operator](https://docs.gitlab.com/operator/troubleshooting.html) if you have any issues while installing and configuring GitLab Operator and consider improving it.

## Stage 6: Tickets

1. [ ] Find 5 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 2 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __

## [OPTIONAL] Stage 7: Pair on customer calls

1. [ ] Pair on calls, where a customer is having trouble with GitLab or GitLab Runner on OpenShift.
   1. [ ] call with ___

## Stage X: Assessment

Note: Please do not look at the assessment until you are ready to complete it.

- [ ] **Done with Stage X**

1. [ ] Complete [the assessment](). Assessments are stored in the [Support Team Drive Training/Training Module Assessments folder](https://drive.google.com/drive/u/0/folders/147D5ecSDsV2J9OCN_6t-J4vYzPLTebue).
  - If you are linked to a Google doc, please make a copy, answer the questions, then share with your trainer or an expert when complete. They will assess your answers and give you feedback.
  - If you are linked to a Google form, please complete the self-assessment. If you have any questions about the answers, please ask your trainer or an expert.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself!

1. [ ] Update ...

## Final stage: Completion

1. [ ] Make sure to remove all the objects that you created in GKE while working on this module.
1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module

/label ~"Module::OpenShift"
/assign me
