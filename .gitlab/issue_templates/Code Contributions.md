---
module-name: "code-contributions"
area: "Product Knowledge"
gitlab-group: "Create:Editor"
maintainers:
  - anton
  - manuelgrabowski
  - rvzon
---

### Overview

**Goal**: You feel proficient at making a code merge request to the [GitLab project](https://gitlab.com/gitlab-org/gitlab).

**Objectives**: At the end of this module, you should be able to:

- open a merge request specifically for a code contribution.
- test your code locally.
- follow the contribution process.

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: Code Contributions - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: Code Contributions
     level: 2
   ```

### Stage 1: Getting to your first MR

The guidelines and workflow:

1. [ ] Take a look at [our development guidelines](https://docs.gitlab.com/ee/development/). For now, stick with the main page and the table of contents to get a sense of what is included.
1. [ ] Start with reading the [Merge request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html), with special attention to the [commit message guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-message-template).

Getting changes done:

1. [ ] Get your [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit#getting-started) running. If you have any issues, post in the #gdk Slack channel for help.
    - Alternatively, if you do not need to connect your GDK to any integrations (runner, cluster, auth), you can use [GitPod](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/gitpod.md).
1. [ ] Choose something to work on. Link the issue or related ticket:
    - For your first MR, we recommend choosing something simple that would not involve spec files, database migrations, etc. unless you're experienced in those areas.
    - If you don't have anything ready, some possibilities include:
        - removing a [manual Rubocop exclusion](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.rubocop_manual_todo.yml) as explained [for this Hackathon (gitlab#239356)](https://gitlab.com/gitlab-org/gitlab/-/issues/239356) and adhering the newly flagged codes to the RuboCop rule,
        - issue on updating code based on ruby version or warnings (such as [kwarg deprecation warnings](https://gitlab.com/gitlab-org/gitlab/-/issues/257438)),
        - resolving a ["static code analysis" labelled issue](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=static%20code%20analysis),
        - issue or epic on migration components (similar to [gitlab&3951](https://gitlab.com/groups/gitlab-org/-/epics/3951)),
        - consider a [Support Priority issue](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Support%20Priority) (though these may or may not be good for new contributors),
        - ask the team, especially your trainer, a senior, or your manager,
        - check for [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=quick%20win&first_page_size=100) with the `~quick win` label; these usually have an "implementation plan" in their description that will contain helpful details
1. [ ] While the merge request workflow says to fork the project, you can instead go into the `/gitlab` directly inside your GDK repo and create a feature branch.
1. [ ] After you've made the code changes, make sure to preview it using your GDK.
1. [ ] Make sure you have [lefthook installed](https://docs.gitlab.com/ee/development/contributing/style_guides.html). It will run the tests you need before you push, and take a read over the configuration to understand what it checks.
    - The [style guides page](https://docs.gitlab.com/ee/development/contributing/style_guides.html) also covers how to run the tests individually. Typically, you want to run danger, and RuboCop.
1. [ ] Read over [when and how to make a changelog entry](https://docs.gitlab.com/ee/development/changelog.html). If your MR requires a changelog, amend your commit to include the `Changelog` trailer.

Getting your first merge request in:

1. [ ] When you're ready, push your changes.
1. [ ] When creating the merge request, fill in as much of the template information as you can. Some of these you can fill in later instead of at creation time.
    1. [ ] Link to the issue and/or ticket:
        - If there is an existing issue, assign yourself to the issue and add the label ~"workflow:in dev". You can also use the `/copy_metadata #123` [quick action](https://docs.gitlab.com/ee/user/project/quick_actions.html) to copy labels, milestone, etc.
    1. [ ] Labels: add [appropriate labels](https://handbook.gitlab.com/handbook/support/workflows/working-with-issues/#adding-labels) including relevant [support team labels](https://handbook.gitlab.com/handbook/support/engineering/#support-team-contributions).
    1. [ ] Milestone: Next release number (or the one after if you don't think it'll be merged by the next one's due date).
1. [ ] Read over the [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) paying special attention to [getting and assigning your MR for review](https://docs.gitlab.com/ee/development/code_review.html#having-your-merge-request-reviewed).
1. [ ] Preview your change using [the review app](https://docs.gitlab.com/ee/development/testing_guide/review_apps.html).
1. Once your MR is merged, you will want to test it on:
    1. [ ] [staging](http://staging.gitlab.com/). Use the Google Login to create an account if needed.

### Stage 2: Review other contributions

1. Review 5 of these previous code contributions from Support team members to get an idea of the variety of code contributions, and a better sense of the process. It's also a good idea to check the previous commits of the files you wish to change. You might find a previous commit that is similar to what you'd like to implement.
Note: If you find better examples, feel free to edit this list.
   1. [ ] Update to rename a label: [Renaming the rules label to conditions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112650)
   1. [ ] Update conditional UI text in vue: [Add delay user deletion UI text](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124251)
   1. [ ] View change: [Display Enterprise group ID and timestamp](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132938)
   1. [ ] Logic update in the UI: [Correct the logic on duplicate package toggles](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92435)
   1. [ ] Update change with spec changes: [Export last activity details - seat usage quotas page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132612)
   1. [ ] Bug fix for a wildcard redirect in GitLab Pages: [Don't redirect acme challenge](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/819)
   1. [ ] Refactor service with multiple specs: [Refactor activate service and add an audit event](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121708)
   1. [ ] Adding new API endpoint that went through multiple iterations and AppSec: [Add PATCH /users/:id/disable_two_factor endpoint](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/88799)
   1. [ ] More complex bug fix to allow project transfers from one namespace to another after removing NPM packages: [Fail TransferService only if there are namespaced npm packages](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95852)
   1. [ ] Add audit event for users with inactive SCIM: [Add audit events for the removal of users with inactive SCIM id from a group](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173879)
   1. [ ] Add audit event for the deletion of unconfirmed provisioned users: [Link user_destroyed event for user provisioned by group to the group](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176405)

Some older examples that might still be interesting:

   1. [ ] Database performance (N+1) example with specs: [Looking up solo_owned_groups for a user N+1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/48340)
   1. [ ] Adding to helper files: [Add RHEL 8 to helper and SELinux files](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/4501)
   1. [ ] Adding rake task involving concerns and spec: [Rake task to verify secrets using gitlab:doctor:secrets](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/21851)
   1. [ ] More complex example: [Add SELinux module for gitlab-shell](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/4598)
   1. [ ] More complex example involving improving query performance, with feature flags and specs: [Allow secondary emails when searching user emails](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/47587), with [feature flag rollout issue](https://gitlab.com/gitlab-org/gitlab/-/issues/282137) and [follow-up MR to remove feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/49312)


### Stage 3: Contribute!

Beyond the very basics, you may make changes that touch spec files, the database, or other parts of the codebase. Some additional reading:

1. [ ] If it's a significant feature or improvement change, you will want to put it behind [a feature flag](https://docs.gitlab.com/ee/development/feature_flags/). Take a look at when and how to use a FF.
1. [ ] Read over the [testing best practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html) with focus on RSpec.
1. [ ] If your changes require database changes, you will need to [review the database development guidelines](https://docs.gitlab.com/ee/development/database/).
1. [ ] Consider watching [this video](https://www.youtube.com/watch?v=yGIKs_7vOvg) for a demonstration of an example code contribution involving database migrations, spec files, and feature flags.


Keep in mind that while this module specifically covers the GitLab project, the process is generally the same for all projects in [gitlab-org](https://gitlab.com/gitlab-org), so you can link to work done in other projects. Some of the other projects can have additional development/testing requirements or guidelines, so it's recommended to read through the relevant README.

1. Link 2 submitted merge requests:
   1. [ ] __
   1. [ ] __

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- open a merge request specifically for a code contribution.
- test your code locally.
- follow the contribution process.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] If you have feedback on whether the [issue suggested above for first MRs](#stage-1-getting-to-your-first-mr)
      were suitable or not, please do so in
      [support-team-meta#3084](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3084).
- [ ] Consider adding your own merge requests to the list of MRs in stage 2.
- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask a support code contributor to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ]  Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).
1. [ ] Consider also taking the [Documentation training](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Documentation.md) which focuses on GitLab docs changes.

/label ~module
/label ~"Module::Code Contributions"
/assign me
