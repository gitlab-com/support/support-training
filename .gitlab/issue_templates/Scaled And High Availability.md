---
module-name: "Scaled And High Availability"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - TBD
---

## Overview

**Goal**: Set a clear path for High Availability Expert training

**Objectives**: At the end of this module, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this module>

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

In addition to the suggested actions here you might like to refer to [Wei-Meng's HA book](https://gitlab.com/weimeng/gitlab-ha-book/-/blob/master/gitlab-ha.ad) (currently WIP 2020-03)

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GitLab HA - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical HA questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Commit and Become familiar with what High Availability is

- [ ] **Done with Stage 1**

1. [ ] Understand the [basic concept of high availability](https://www.digitalocean.com/community/tutorials/what-is-high-availability)
1. [ ] Understand the use of [GitLab High Availability roles](https://docs.gitlab.com/omnibus/roles/)
1. [ ] Understand the different [HA architectures possible with GitLab](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#supported-modifications-for-lower-user-counts-ha)

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. [ ] Set up a minimal [HA setup using the GitLab omnibus package](https://docs.gitlab.com/ee/administration/high_availability/)
   1. [ ] [Configure the database](https://docs.gitlab.com/ee/administration/high_availability/database.html)
   1. [ ] [Configure Redis](https://docs.gitlab.com/ee/administration/high_availability/redis.html)
   1. [ ] [Configure object storage](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#configure-the-object-storage)
   1. [ ] [Configure the GitLab Application servers](https://docs.gitlab.com/ee/administration/high_availability/gitlab.html)
   1. [ ] [Configure the load balancers](https://docs.gitlab.com/ee/administration/high_availability/load_balancer.html)

Note: A minimal setup should have 2 application servers, 7 nodes for database components and 1 load balancer.

## Stage 3 (WIP): GitLab HA Administration

- [ ] **Done with Stage 3**

Remember to contribute to any documentation that needs updating

## Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 5 (WIP): Customer Calls

- [ ] **~~Done with Stage 5~~**

## Stage 6 (WIP): Quiz?

- [ ] **~~Done with Stage 6~~**

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in High Availability on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module
/label ~"Module::Scaled And High Availability"
/assign me
