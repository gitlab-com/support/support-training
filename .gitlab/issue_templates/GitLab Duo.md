---
module-name: "GitLab Duo"
area: "Product Knowledge"
gitlab-group: '"AI Framework" "Cloud Connector" "Duo Chat" "Editor Extensions" "Code Creation"'
maintainers:
  - Austin_Pierce
---

## Introduction

This training module is intended to provide Support Engineers with a better understanding of GitLab Duo and the underlying AI technology.

**Goals of this training module**

At the end of this module, you should be able to:
- answer Duo-related tickets (or have a good idea of how to start troubleshooting Duo-related tickets/problems)
- demonstrate an understanding of the role Duo plays in GitLab
- support peers with questions about Duo

**General Timeline and Expectations**

- This issue should take you **TBD to complete**.

Reminders:

- This is a **Public** issue; don't include anything confidential.
- You should do stage **0** first and the final stage last. Outside of that, you can perform tasks from the stages in any order you like.

### Stage 0: Create Your module

1. [ ] Create an issue using this template by making the Issue Title: "Learning GitLab Duo - your name"
1. [ ] Add yourself as the assignee
1. [ ] Consider setting a milestone and/or a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) file to indicate that you've started learning this knowledge area:

```yaml
knowledge_areas:
- name: GitLab Duo
  level: 1
```

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 0.5: Foundational Concepts

- [ ] **Done with Stage 0.5**

**Goal** Gain a basic understanding of the underlying AI technologies and methods that power GitLab Duo.

1. [ ] Watch [What is Machine Learning?](https://www.youtube.com/watch?v=KHbwOetbmbs) (approx. 3 minutes)
1. [ ] Watch [How Neural Networks Work](https://www.youtube.com/watch?v=JrXazCEACVo) (approx. 5 minutes)
1. [ ] Watch [How Chatbots and Large Language Models Work](https://www.youtube.com/watch?v=X-AWdfSFCHQ) (approx. 8 minutes)

For further exploration, consider the following resources:

* AWS Cloud Computing Concepts Hub: [Deep Learning](https://aws.amazon.com/what-is/deep-learning/)
* AWS Cloud Computing Concepts Hub: [Large Language Models (LLMs)](https://aws.amazon.com/what-is/large-language-model/)
* Khan Academy/Code.org Computing Unit 2: [How AI works](https://www.khanacademy.org/computing/code-org/x06130d92:how-ai-works)

### Stage 1. Overview of GitLab Duo

- [ ] **Done with Stage 1**

**Goal** Understand the basics of what GitLab Duo does, what is supported, how to purchase, and how data collected is used.

1. [ ] Read [the GitLab Duo overview](https://docs.gitlab.com/ee/user/gitlab_duo/) to familiarize yourself with the generally available (GA), beta, and experimental Duo features.
1. [ ] Read GitLab's [Support policy for experiment, beta, and GA features](https://docs.gitlab.com/ee/policy/experiment-beta-support.html) to understand the level of support provided for each.
1. [ ] Read [Subscription Add-ons](https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html) to understand how customers purchase Duo and assign Duo seats to users on GitLab.com and self-managed instances.
1. [ ] Learn about Duo Enterprise, a higher-tier Duo add-on. Note that only Ultimate customers are eligible to purchase Duo Enterprise.
    1. [GitLab Duo Enterprise Blog Post](https://about.gitlab.com/blog/2024/09/03/gitlab-duo-enterprise-is-now-available/)
    1. [View a walkthrough of GitLab Duo Enterprise features](https://gitlab.navattic.com/duo-enterprise)
1. [ ] Read about [GitLab Duo data usage](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html) to learn how customer data is used (or not).

#### Keeping up with changes

GitLab is moving fast on AI. Below are some ways you can keep up with recent and upcoming developments.

1. [ ] Consider subscribing to the [AI Powered Weekly Updates](https://gitlab.com/gitlab-org/ai-powered/ai-weekly/-/labels?subscribed=&sort=relevance&search=+AI+powered+weekly+updates) label
1. [ ] Consider turning on notifications for new [#spt_pod_ai](https://gitlab.enterprise.slack.com/archives/C06KMDBJT5F) posts 
1. [ ] Review release notes for new major and minor versions of GitLab. As of writing this in July 2024, there's a very good chance that Duo improvements and additions will be in each release.
1. [ ] Consider adding some of these Slack channels
    - `#ai_strategy`
    - `#s_ai-powered`
    - The Slack channel for any AI product group you're particularly interested in. For example:
        - `#g_duo_chat`
        - `#g_editor-extensions`
        - `#g_code_creation`
        - `#g_ai_framework`
        - `#g_cloud_connector`
1. [ ] Consider attending All Hands meetings, where AI announcements and progress are discussed.


### Stage 2. GitLab Duo in practice

- [ ] **Done with Stage 2**

**Goal** Learn how to configure GitLab Duo, and practice using core features like Chat and Code Suggestions.

#### Set up a self-managed instance and enable GitLab Duo

**If you already have a working Sandbox Cloud instance on GitLab 17.3 or higher, skip steps 1-2.**

1. [ ] If you haven't done so already, take the time to get acquainted with the [GitLab Sandbox Cloud](https://handbook.gitlab.com/handbook/support/workflows/test_env/#gitlab-sandbox-cloud-for-gcp-preferred). Ensure you're capable of deploying a basic Omnibus instance.
1. [ ] Using GitLab Sandbox Cloud, deploy a single node Omnibus instance on version `17.3.0` or higher. The template `support-resources-template-v2-***` can be used for that. Confirm that it has at least a Premium license associated with it. 
1. [ ] Follow the instructions [to obtain a GitLab Duo license](https://docs.gitlab.com/ee/development/code_suggestions/#setup-instructions-to-use-gdk-with-the-code-suggestions-add-on) and apply it to your instance. (The linked documentation is for GDK instances, but it also works for standard Omnibus instances.)
    - If Duo features don't work after adding the license, try [manually synchronizing your subscription](https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#manually-synchronize-subscription-data). The initial automatic synchronization [can take up to 24 hours](https://docs.gitlab.com/ee/user/gitlab_duo_chat/turn_on_off.html#manually-synchronize-your-subscription).  
1. [ ] Read [Configure GitLab Duo on a self-managed instance](https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#configure-gitlab-duo-on-a-self-managed-instance). By default, a basic Omnibus instance deployed with GitLab Sandbox Cloud meets the connectivity requirements for a self-managed instance, so you shouldn't have to change anything.


#### Control GitLab Duo access

1. [ ] For some GitLab Duo features, like Code Suggestions, you must assign seats to users for them to be able to use the features. Create a user on your instance, and [assign a Duo Pro seat](https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#for-self-managed) to both your `root` user and newly created user.  
1. [ ] Read the introductory section of [Turn off GitLab Duo features](https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#turn-off-gitlab-duo-features).
1. [ ] Practice [turning Duo off and on](https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#turn-off-gitlab-duo-features) at the:
    1. [ ] Group level
    1. [ ] Project level
    1. [ ] Instance level
1. [ ] [Turn on beta and experimental features](https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#turn-on-beta-and-experimental-features) on your instance. 

#### Try Duo Chat

1. [ ] Read about [The context Duo Chat is aware of](https://docs.gitlab.com/ee/user/gitlab_duo_chat/#the-context-chat-is-aware-of).
    - [ ] Go to [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/471277), open Duo Chat on that page, and ask a question about the issue. 
    - [ ] Go to [this file](https://gitlab.com/gitlab-org/gitlab/-/blob/308b146cef03303ccdd9f00a8c94ce3e9b0280db/ee/app/services/llm/internal/completion_service.rb), open Duo Chat on that page, and ask a question about the file.
1. [ ] Review [Ask Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html). Try out three different uses of Duo, and record which ones you tried:
    - [ ] Use:
    - [ ] Use:
    - [ ] Use: 
1. [ ] Review the error codes in [Duo Chat troubleshooting](https://docs.gitlab.com/ee/user/gitlab_duo_chat/troubleshooting.html).

#### Try Duo Code Suggestions and Explanation in the IDE

1. [ ] Read [the docs overview of Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)
1. [ ] Read about [supported extensions and languages](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html)
1. [ ] Configure GitLab Duo in the VSCode IDE 
    1. [ ] Click on the VSCODE IDE editor extension link under [Supported Editor Extensions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-editor-extensions). Follow the instructions for configuring the GitLab Duo extension, and connect it to your Duo-enabled self-managed instance. If you have questions or trouble setting up the extension, don't hesitate to ask for help in [#spt_pod_ai](https://gitlab.enterprise.slack.com/archives/C06KMDBJT5F).
1. [ ] Once your IDE is configured to use GitLab Duo:
    1. [ ] Do one or more of our documented [Code generation prompts](https://docs.gitlab.com/ee/user/gitlab_duo/use_cases.html#code-generation-prompts) in the language of your choice.
    1. [ ] **Optional.** if you want to dive deeper, do one of the [documented development challenges](https://docs.gitlab.com/ee/user/gitlab_duo/use_cases.html#use-gitlab-duo-to-solve-development-challenges) or code along with a [GitLab Duo Coffee Chat recording](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ).  

#### Try out other GitLab Duo features

1. [ ] **Optional.** There are [several more Duo features](https://docs.gitlab.com/ee/user/gitlab_duo). Try out one or more of them to further familiarize yourself with the capabilities of GitLab Duo.


### Stage 3. Understanding the GitLab Duo Infrastructure 

- [ ] **Done with Stage 3**

**Goal** Gain an understanding of the technical architecture of GitLab Duo.

1. [ ] Read [AI Architecture](https://docs.gitlab.com/ee/development/ai_architecture.html) to learn about the AI Gateway, the model-agnostic abraction layer built for developing Duo features.
  1. [ ] Optional: Read the [AI Gateway blueprint](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/index.html) to learn about the historical motivations and goals of this architecture.
1. [ ] Read [Cloud Connector Architecture](https://docs.gitlab.com/ee/development/cloud_connector/architecture.html) to understand how the AI Gateway handles traffic from GitLab.com, self-managed instances, and Dedicated instances.
    1. [ ] Optional: Bookmark and watch [Life of a Cloud Connector Request](https://www.youtube.com/watch?v=DeTh9dhDrnw) on GitLab Unfiltered (recorded September 2024)
1. [ ] Read [the introduction to the GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp#introduction), a common interface for all GitLab IDE extensions.
1. [ ] Read about [GitLab Duo data usage](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html).
1. [ ] Consider reading about [Action Cable](https://guides.rubyonrails.org/action_cable_overview.html#what-is-action-cable-questionmark)
    - Action Cable is used to integrate websockets with the rails application. Subscriptions are the real-time websocket channels that communicate with rails. This is how communication is done with the Duo Chat feature.
    - Note that ActionCable connection and subscription events are logged in `production_json.log`


### Stage 4. Handling GitLab Duo tickets

- [ ] **Done with Stage 4**

**Goal** Learn how to troubleshoot Duo, the scope of support we offer for Duo features, the product groups involved in Duo, and where to get help with difficult Duo-related tickets

#### Troubleshooting GitLab Duo
 
1. [ ] Review and bookmark the [GitLab Duo Triage runbook](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/duo?ref_type=heads). This is an excellent resource for troubleshooting a wide range of Duo-related issues. 
1. [ ] Review [running a health check for GitLab Duo](https://docs.gitlab.com/ee/user/gitlab_duo/setup.html#run-a-health-check-for-gitlab-duo)
    1. [ ] Run a health check on the self-managed instance you set up in Stage 2
1. [ ] The editor extension.
    1. Many Duo-related customer tickets are about questions or issues with the editor extensions. It's important to understand how to use the core troubleshooting options available for these types of problems. 
    1. [ ] Review [Troubleshooting Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html)
    1. [ ] Review [this ticket](https://gitlab.zendesk.com/agent/tickets/533610), noting the troubleshooting steps suggested by the Support Engineer.
    1. [ ] Try [enabling and reviewing](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html#view-code-suggestions-logs) the GitLab:Debug logs and Language Server debugs logs. 
1. [ ] Duo Chat
    1. [ ] Review [this ticket](https://gitlab.zendesk.com/agent/tickets/534622), and take special note of the role licenses can play in troubleshooting Duo.
    1. [ ] Review [this ticket](https://gitlab.zendesk.com/agent/tickets/537922), which highlights how important it is to pay attention to the GitLab version when troubleshooting a Duo feature. This is true for any part of the product, but is especially true for Duo at the moment, where major feature additions and changes are rolling out quickly. 
1. [ ] Review the description section of https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18329, and [this comment](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18329#note_2015832799) from the AI Framework Engineering Manager, David O'Regan. Take note of the common trends and pain points identified from Duo incidents.
1. [ ] Know where to look in Kibana to identify issues
    1. [ ] Consider checking out the [Duo Chat dashboard](https://log.gprd.gitlab.net/app/r/s/SwsYz)
    1. [ ] Check out the `ApplicationCable::Connection` controller in the rails logs (`production_json.log`)
1. [ ] **Optional but recommended.** Search Zendesk for closed Duo tickets, and review 3-4 of them. This will give you a sense of the Duo-related problems customers encounter, and how to troubleshoot them. (Use search keywords like "Duo", "Duo Pro", "Duo Chat", "Code suggestions", etc.)
1. [ ] **Optional.** Update the docs and/or one of the [Duo runbooks](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/duo?ref_type=heads) to clarify how a Duo feature works, or add/update a troubleshooting tip for a Duo feature.
   1. [ ] Docs MR:
   1. [ ] Docs MR:

#### Scope of support for Duo features 

1. [ ] Read the [GitLab Duo section of the Statement of Support](https://about.gitlab.com/support/statement-of-support/#gitlab-duo). Note that customer problems with "the accuracy of, \[or] the responses generated by GitLab Duo Pro" are out of scope.

#### Where to go for help

1. You can ask for help in the [#spt_pod_ai](https://gitlab.enterprise.slack.com/archives/C06KMDBJT5F) channel in Slack
1. Duo is developed across multiple product groups, and sometimes it's hard to figure out which group to ask for help. [This is a known challenge](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18329#:~:text=Debugging%20through%20the%20stack%2C%20identifying%20root%20cause%2C%20and%20determining%20team%20ownership%20are%20both%20difficult%20and%20unclear) that the AI teams are trying to address. In the meantime:
    1. [ ] Bookmark the [Features by Group](https://handbook.gitlab.com/handbook/product/categories/features/) page. It can help you determine which group(s) own any given **generally available** Duo feature.
    1. [ ] Don't worry too much about asking the wrong group. Do your best and you'll be redirected if needed.
1. Knowing about the key AI groups, how they're connected to each other, and how to contact them is invaluable when navigating a problem with Duo. Note that this is _not_ an exhaustive list of groups that own AI features.
    1. [ ] Review the [Core Platform Section > Systems > Cloud Connector group](https://handbook.gitlab.com/handbook/product/categories/features/#systems-cloud-connector-group), which ensures that Duo features are available across all deployment types (GitLab.com, self-managed, and Dedicated).
    1. [ ] Review the organizational charts of the [Data Science section > AI-powered stage](https://handbook.gitlab.com/handbook/product/categories/#data-science-section), and the [Dev section > Create stage](https://handbook.gitlab.com/handbook/product/categories/#dev-section).
    1. The AI-powered stage includes two groups involved in work relevant to Support:
        1. [ ] Review the [Duo Chat group](https://handbook.gitlab.com/handbook/product/categories/features/#ai-powered-duo-chat-group) group, which owns (no surprise) Duo Chat.         
        1. [ ] Review the [AI Framework group](https://handbook.gitlab.com/handbook/product/categories/features/#ai-powered-ai-framework-group), which owns the AI Gateway (a model-agnostic abstraction layer used by other product groups to develop Duo features) and Duo settings. 
        1. [Custom Models](https://handbook.gitlab.com/handbook/product/categories/features/#ai-powered-custom-models-group) is a future feature that will allow users to customize Duo features, including training Duo on their own data. You can read more in [Group direction - Custom Models](https://about.gitlab.com/direction/ai-powered/custom_models/). 
    1. The Create stage includes three groups involved in Duo features: 
        1. [ ] Review the [Code Creation group](https://handbook.gitlab.com/handbook/product/categories/features/#create-code-creation-group), and note the features they own.
        1. [ ] Review the [Code Review group](https://handbook.gitlab.com/handbook/product/categories/features/#create-code-review-group), which owns [Suggested Reviewers](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#gitlab-duo-suggested-reviewers).
        1. [ ] Review the [Editor Extensions group](https://handbook.gitlab.com/handbook/product/categories/features/#create-editor-extensions-group), which implements Duo features in various IDEs. 
1. [ ] Note that you can create a [Request for Help](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-find-the-correct-development-section-and-group-to-reach-out-for-help) issue for each of these groups: 
    - [Cloud Connector](https://gitlab.com/gitlab-com/enablement-sub-department/section-enable-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-CloudConnector.md?ref_type=heads)
    - [Duo Chat](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-DuoChat.md?ref_type=heads)
    - [AI Framework](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-aiframework.md?ref_type=heads)
    - [Editor Extensions](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-EditorExtensions.md?ref_type=heads)
    - [Code Creation](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-CodeCreation.md?ref_type=heads)
    - [Code Review](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/blob/main/.gitlab/issue_templates/SupportRequestTemplate-CodeReview.md?ref_type=heads)


### Stage 5. Final Stage

- [ ] **Done with Stage 5**

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   modules
   - GitLab Duo

   knowledge_areas:
   - name: GitLab Duo
     level: 2
   ```
You will now be listed as "Ready to work tickets" in GitLab Duo on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).


## :tada: Congratulations! You made it, and are well-prepared to assist customers and colleagues with Duo-related tickets. :tada:

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).
/label ~"module" ~"Module::GitLab Duo"
/assign me
