---
module-name: "GitLab CLI"
area: "Product Knowledge"
gitlab-group: "Create:Code Review"
maintainers:
  - benjaminking
  - faleksic
---

## Overview

**Goal:** Provide an introductory guide to using [GitLab CLI](https://gitlab.com/gitlab-org/cli).

At the end of this module, you should be able to:

- Explain what GitLab CLI is, and how it allows you to use your terminal to work on issues, merge requests, and monitor pipelines.
- Direct customer queries to the relevant documentation.
- Know where to ask for help.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `GitLab CLI - <your name>`
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing GitLab CLI questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:
   
   ```yaml
   knowledge_areas:
   - name: GitLab CLI
     level: 1
   ```

## Stage 1: Commit and Become familiar with GitLab CLI

- [ ] **Done with Stage 1**

1. [ ] Read the [GitLab CLI Integration Overview](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli//)
1. [ ] Read the [Repository README](https://gitlab.com/gitlab-org/cli/-/blob/main/README.md).
1. [ ] Find answers to and understand the following questions:
  1. [ ] How do I install GitLab CLI?
  1. [ ] How does GitLab CLI authenticate with GitLab?
  1. [ ] What actions are supported using GitLab CLI?

### Stage 2: Install GitLab CLI

- [ ] **Done with Stage 2**

Remember to contribute to any documentation that needs updating.

1. [ ] [Follow the instructions](https://gitlab.com/gitlab-org/cli/#installation) to install GitLab CLI for your operating system.

1. [ ] Start an interactive setup of the tool with `glab auth login`. At the prompts:
  1. [ ] Select either `gitlab.com` or a self-hosted instance.
  1. [ ] Generate a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` and `write_repository` scopes. Consider setting a short expiration date.
  1. [ ] Select either `ssh`, `https`, or `http` as the default git protocol.

1. [ ] Create your own example project on GitLab (using the same platform you setup a token on previously), then clone the project using `glab repo clone`.

### Stage 3: Merge Requests with GitLab CLI

- [ ] **Done with Stage 3**

1. [ ] Create a new branch from the project with `git checkout -b new-feature`. 
1. [ ] Make a change to the `README.md` file. 
1. [ ] Use `git add`, `git commit` and `git push` to save and push your changes.
1. [ ] Use `glab mr create` to create MR based off your changes in the `new-feature` branch.
  - You will be prompted for the `Title` and `Description`, which you can provide interactively. Additional metadata can also be provided. Once ready, select `Submit`.
1. [ ] Once the MR has been created, you can now list, view and update the MR, before optionally approving, then merging it.
  1. [ ] Use `glab mr list` to see a list of available MRs on the project. Your most recent MR should be available.
  1. [ ] Use `glab mr view <MR number>` to view details about the MR.
  1. [ ] Use `glab mr note -m <message>` to add a note to the MR.
  1. [ ] Use `glab mr merge` to launch an interactive prompt to merge the changes. You will be able to define what type of [merge method](https://docs.gitlab.com/ee/user/project/merge_requests/methods/) to use.

### Stage 4: Issues with GitLab CLI

- [ ] **Done with Stage 4**

1. [ ] Create a new issue with `glab issue create`.
  - You will be prompted for the `Title` and `Description`, which you can provide interactively.
  1. [ ] With the issue created, you can now list, view, update and close the issue via your terminal.
    1. [ ] Use `glab issue list` to display current issues, which will include the one you just created.
    1. [ ] Use `glab issue view <issue number>` to view details about the issue.
    1. [ ] Use `glab issue note -m "Closing as this was resolved in a recent merge" <issue number>`
    1. [ ] Use `glab issue close <issue number>` to close the issue.


### Stage 5: Monitoring CI with GitLab CLI

- [ ] **Done with Stage 5**

1. [ ] Create a new branch in the project to include an example `.gitlab-ci.yml` file [provided in the GitLab documentation](https://docs.gitlab.com/ee/ci/quick_start/#create-a-gitlab-ciyml-file). Try updating this by using your terminal. For additional practice, consider creating an MR, then merging it to `main`, using GitLab CLI and what you learned earlier in this module.
1. [ ] Verify that the `.gitlab-ci.yml` is valid by using the `glab ci lint` command
1. [ ] Once you've committed the changes, you can view and monitor the pipeline progress in your terminal.
  1. [ ] Use `glab ci list` to display all pipelines on the project.
  1. [ ] Use `glab ci view` to monitor the most recent triggered pipeline. As the jobs commence, they will change colour to indicate a success or failure.
  1. [ ] Use `glab ci status` to bring up an interactive prompt. Follow the prompts to view logs from the pipeline in your terminal.
  1. [ ] Use `glab ci run` to trigger a new pipeline off the current branch.

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- Familiar with GitLab CLI
- Can use basic `glab` commands to navigate issues, merge requests, and pipelines using a terminal.
- Know where to look for further documentation, or to report bugs.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

### Final Stage

1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```yaml
   knowledge_areas:
   - name: GitLab CLI
     level: 2
   ```

/label ~module
/label ~"Module::GitLab CLI"
/assign me