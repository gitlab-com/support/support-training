---
module-name: "BPO Onboarding"
area: "Customer Service"
maintainer:
  - lyle 
---

# Introduction

Welcome to the Support Team, we are so excited that you've joined us!

As an employee of a BPO, you'll be operating on a subset of the tickets and use cases Support handles - but having a deep familiarity with the company, our Values and our ways of working will help make sure that we're providing the same excellent quality of Support that our customers and users expect.

This module is organized into two parts:
- Esseentials: These are the critical things to know about GitLab and the work you'll be doing. Whether you're cross-training, or being permanently assigned this is the place to start.
- Going Deeper: If you're being permanently assigned to support GitLab, you'll want to spend a bit more time going through our onboarding so you can get the context you need to serve our customers and connect with your teammates.

**Goals of this Support Team Member Onboarding Issue**

This Issue is an tracking issue with a checklist of all the relevant areas of the [GitLab Onboarding](https://handbook.gitlab.com/handbook/people-group/general-onboarding/) and Learning Modules that comprise Support-specific Onboarding. Keep this issue open until you complete all the tasks and modules identified for your role.

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/).
- Your [Support Onboarding Buddy](https://handbook.gitlab.com/handbook/support/training/onboarding_buddy/) is your primary contact-person during your onboarding. They will schedule regular check-ins with you and will be able to guide you through your [Onboarding Modules](https://handbook.gitlab.com/handbook/support/training/#support-onboarding-pathway) at least until your onboarding tasks have been completed. Once onboarding has finished, continued pairing with the Onboarding Buddy is always encouraged but at the support engineer's discretion. 

# Essentials

Required for all BPO staff assigned to GitLab's account, whether cross-trained or permanently assigned.

### Learn about the company and team
1. [ ] Read our [Mission](https://handbook.gitlab.com/handbook/company/mission/)
1. [ ] Take a look at [Some of our customers](https://about.gitlab.com/customers/)
1. [ ] Understand [the products that we sell](https://handbook.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/tiers/)
1. [ ] Read [about the Support Team](https://handbook.gitlab.com/handbook/support/)
1. [ ] Understand [what Support looks like for customers](https://about.gitlab.com/support/)

### Learn about how GitLab operates

1. [ ] Take the [GitLab Values Training](https://university.gitlab.com/learn/course/gitlab-values-training/gitlab-values-training/)

### Learn the essential skills GitLab Support requires

Each of the modules listed below has an issue template in our [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/main/.gitlab/issue_templates). To create your training issue, create a new issue using the links below, follow the instructions for the title, assign it to yourself, and save.

**NOTE:** When you instantiate one of the modules, replace the placeholder link below with a direct link to the new Issue ID.

1. [ ] Customer Service Skills (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Customer%20Service%20Skills" target="_blank">create new issue</a>)
1. [ ] GitLab Support Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab%20Support%20Basics" target="_blank">create new issue</a>)
1. [ ] Zendesk Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Zendesk%20Basics" target="_blank">create new issue</a>)
1. [ ] Working on Tickets (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Working%20On%20Tickets" target="_blank">create new issue</a>)

# Going Deeper

### Meet your Onboarding Buddy

Your Support Onboarding Buddy is: `PERSON`

1. [ ] Tag `@lyle` to be assigned an Onboarding Buddy
1. [ ] Schedule a 30m call to meet, get to know one another and get any questions have answered.
1. [ ] Feel free to schedule follow-up calls for pairing, additional questions or just to connect and talk through things!

### Learn about how GitLab operates

1. [ ] Read through [GitLab's All Remote section of the Handbook](https://handbook.gitlab.com/handbook/company/culture/all-remote/getting-started/)
1. [ ] Familiarize yourself with how GitLab communicates by reading [Communication - Handbook](https://handbook.gitlab.com/handbook/communication/)

#### Optional:
1. [ ] Get [certified in TeamOps](https://university.gitlab.com/learn/course/teamops)

### Meet some team members (can be completed at any time)
1. [ ] Read about [coffee chats](https://handbook.gitlab.com/handbook/company/culture/all-remote/informal-communication/#coffee-chats)
1. [ ] Explore the [internal Support Team page](https://gitlab-support-readiness.gitlab.io/support-team/) to see team members in Support, as well as their scheduling link.
1. [ ] Schedule 5 coffee chats:
   - [ ] Person 1
   - [ ] Person 2
   - [ ] Person 3
   - [ ] Person 4
   - [ ] Person 5
1. [ ] If you'd like to be automatically paired with Support Team members, request to join the [#spt_donut](https://gitlab.slack.com/archives/C01AL6L262J) 

### Learn about GitLab (the product)
1. [ ] Sign up for [GitLab University](https://university.gitlab.com/) by clicking the "Sign in" or "Start learning" buttons and signing in with your GitLab.com account
1. [ ] Tag `@lyle` to assign the following courses to you:
   1. [ ] Take [The GitLab For Team Members Certification](https://university.gitlab.com/learn/course/gitlab-team-members-certification/main/gitlab-team-members-certification)
   1. [ ] Learn about [How to Edit the Handbook](https://handbook.gitlab.com/handbook/practical-handbook-edits/)


### Go deeper in GitLab Support essential skills

Each of the modules listed below has an issue template in our [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/main/.gitlab/issue_templates). To create your training issue, create a new issue using the links below, follow the instructions for the title, assign it to yourself, and save.

**NOTE:** When you instantiate one of the modules, replace the placeholder link below with a direct link to the new Issue ID.

1. [ ] Documentation (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Documentation" target="_blank">create new issue</a>)

## Final Steps, Onboarding Feedback & Documentation

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past. Giving feedback about your onboarding experience will ensure that this document is always up to date, and those coming after you will have an easier time coming in.

1. [ ] Make an update to one or more Support training module templates to make it better and link them below. The files are located in an issue template in the [support-training repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates). Have [a maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).
      1. \_________


#### Congratulations on completing your Onboarding modules successfully!

/label ~onboarding ~Module::BPO-Onboarding
