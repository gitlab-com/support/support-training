---
module-name: "cross-platform-training"
area: "Customer Service"
maintainers:
  - ralfaro
---

## Overview

**Goal**: Given a familiar problem, you will know how to gather the information necessary to troubleshoot.

**Objectives**: At the end of this module, you should:

- know where to look for logs.
- know what information you need to ask a customer for.
- be able to troubleshoot issues for familiar problems on the platform you weren't familiar with.

Some additional context: The emphasis of this training is to be able to solve problems you already know how to solve,
except on the platform (Self-managed or SaaS) which you may not be (as) familiar with.
You are not expected to be solving new types of problems.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `Cross-platform training - <platform you are less familiar with: SM or SaaS> - <your name>`
1. [ ] Remove the Stage 1 that doesn't apply below. For example, if you need to learn SaaS, remove the "Stage 1b: Self-managed version".
1. [ ] Choose a trainer by finding a team member who is [familiar with the platform that you need to learn more about](https://gitlab-support-readiness.gitlab.io/support-team/areas-of-focus.html), is familiar with [at least 1 area that you're also knowledgeable in](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-subject.html), and overlaps at least a half day in work hours.
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set a Due Date to help keep you on track. Suggested: 2 weeks.
1. [ ] Notify your manager to let them know you've started.

## Stage 1a: SaaS (GitLab.com) version

1. [ ] Open an issue for the [GitLab.com SaaS Basics training](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab-com%20SaaS%20Basics): link
1. [ ] Remove Stage 4, 5, and 6 or mark them as "N/A".
1. [ ] The focus of the training is on Stage 3, so in Stage 2, optionally mark any task that requires you to complete a ticket or issue with a hyphen `[-]`.

## Stage 1b: Self-managed (SM) version

1. [ ] Open an issue for the [GitLab Installation and Admin Basics](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab%20Installation%20and%20Administration%20Basics) and make it related to this one.
    - Note: If you've already completed that module, make it related and mark this task as done.
1. [ ] Mark all tasks that you're not going to do with a hyphen `[-]` based on the following:
    - Choose 1 method of installation and try it. If it is the GDK, choose a second installation method. All others are optional.
    - Keep 1 type of instance so you can test things. This can be the GDK.
    - All tasks in the "services" section (runner, ElasticSearch, etc.) are optional.
    - Consider coming back to optional tasks later on, or completing more if you are interested or have time.
1. [ ] Open an issue for the [Self-Managed Basics](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Self-Managed%20Basics) and make it related to this one.
1. [ ] Mark all tasks that you're not going to do with a hyphen `[-]` based on the following:
    - Stage 2: Consider 2 calls the minimum.
    - If you have experience with customer calls already, reverse shadowing is not required, but recommended.
    - Patching an instance is optional.

## Stage 2: Tickets & Pairings

When working on tickets, make sure to refile them (Form, Problem Type) if needed to ensure they show up in the correct Area of Focus ZenDesk views.

1. [ ] Recommend but optional: To find tickets in topics that you're familiar with, consider creating a [personal ZenDesk view](https://support.zendesk.com/hc/en-us/articles/4408888828570-Creating-views-to-manage-ticket-workflow) that filters by `Problem Type` (topic) and `Form`.
1. Complete 5 pairing sessions with a Support Engineer who is focused on the platform you're training for. They should be with at least 2 different individuals. Include the topic you focused on.
   1. [ ] Topic: link
   1. [ ] Topic: link
   1. [ ] Topic: link
   1. [ ] Topic: link
   1. [ ] Topic: link
1. List 5 tickets below that you've assigned yourself to and intend to work through to completion where the platform (ZenDesk Form) is the platform you're training for.
   1. [ ] _
   1. [ ] _
   1. [ ] _
   1. [ ] _
   1. [ ] _

## Stage 3: Takeaways

1. [ ] List 1-5 takeaways from this module. These could include things that stood out, things you learned that you believe are crucial to keep in mind, things that everyone should know, etc.
    1. _
    1. _
    1. _
    1. _
    1. _

## Penultimate stage: "Sharing is caring"

1. [ ] Post your takeaways as a comment in:
    - Learning SaaS: https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3468
    - Learning SM: https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3469
1. [ ] Read over at least 1 other engineer's takeaways, and react with an emoji and/or comment.
1. [ ] Are there improvements to be made? Submit a MR, assign your manager or training, and add `@cynthia` as a reviewer. If not, check off this task to indicate you thought about it.

## Final stage: Completion

1. [ ] Open the [tracking spreadsheet at the top of epic 138](https://gitlab.com/groups/gitlab-com/support/-/epics/138), and add the link to this issue in the column that you're training for next to your name. For example, if you finished learning about GitLab.com (SaaS), put it in the `SaaS` column.
1. [ ] Let your manager know you've completed using whichever communication method you'd like so they can complete the next item.
1. [ ] Manager: Please acknowledge you're reviewed this issue by adding a comment on the issue and closing it.

/label ~module
/epic &138
/label ~"Module::Cross-platform training"
/assign me
