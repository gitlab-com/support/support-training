---
module-name: "Docker"
area: "Core Technologies"
maintainers:
  - davinwalker
  - faleksic
---

Tackle stage 1 first and the last stage last, but the others can be completed in any order you prefer.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `Docker - <your name>`
2. [ ] Add yourself and your trainer as the assignees.
3. [ ] Notify your manager to let them know you've started.
4. [ ] Commit to this by notifying the current experts that they can start routing non-technical Docker questions to you.
5. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: docker
     level: 1
   ```

## Stage 1: Theory

- [ ] **Done with Stage 1**

Note: You can expense non-free learning materials to GitLab by following the [How to Apply For Growth and Development Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#how-to-apply-for-growth-and-development-benefits) handbook page.

1. [ ] Understand the difference between a VM and a Docker container by watching [Containers vs VMs: What's the difference?](https://www.youtube.com/watch?v=cjXI-yxqGTI) video.
2. [ ] Watch [GitLab University Docker](https://www.youtube.com/watch?v=ugOrCcbdHko) to understand containers a bit better.
3. [ ] Read the docs for the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/#gitlab-container-registry).
4. [ ] Read the docs for the [GitLab Docker Images](https://docs.gitlab.com/ee/install/docker.html).
5. [ ] Watch Lesson 1 and Lesson 2 of [Scalable Microservices With Kubernetes](https://www.udacity.com/course/scalable-microservices-with-kubernetes--ud615).
6. [ ] (Optional) Consider getting the [Docker and Kubernetes: The Complete Guide](https://www.udemy.com/course/docker-and-kubernetes-the-complete-guide/) if you would like to learn more.

If you've taken the above course you **don't** need to watch it all as it's 20h+ long. If you did please proceed with watching the following sections:

1. [ ] Dive Into Docker!
2. [ ] Manipulating Containers with the Docker Client
3. [ ] Building Custom Images Through Docker Server
4. [ ] Making Real Projects with Docker

Remember, the "Docker and Kubernetes: The Complete Guide" contains useful stuff regarding Kubernetes, this is useful if you take the [Kubernetes Part 1](Kubernetes%20Part%201.md) module later.

## Stage 2: Practical

- [ ] **Done with Stage 2**

Now that we understand what Docker is its time to run some commands!

1. [ ] Install Docker on your machine, consider using alternatives mentioned in our [Handbook | Docker Desktop](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop) page, [podman](https://podman.io/) is also a good alternative.
2. [ ] To check if everything is operational run the [hello-world](https://hub.docker.com/_/hello-world) container to print out a "hello world" message.
3. [ ] Next we will print out some content from a docker container. Run the `ubuntu:latest` image and print out the Ubuntu version with `cat /etc/os-release`.
4. [ ] After running the above command you might notice some containers that are no longer needed with `docker ps -a`.
   1. [ ] Remove the `ubuntu` container by running `docker rm <CONTAINER_ID>` where the `CONTAINER_ID` is the ID from the previous step.
   2. [ ] Confirm that you've deleted it with `docker ps -a`, that container shouldn't be on the list.
5. [ ] Now we will go a bit deeper, we will jump into the container itself, think of it as `ssh`-ing into the container. Read the documentation on [Docker exec](https://docs.docker.com/engine/reference/commandline/exec/#usage).
   1. [ ] Jump into a new `ubuntu:latest` container and install `curl` (`apt update && apt install curl`).
   2. [ ] Within the same container write something in a file named `example.txt` (`echo 'you dont have to write "something" you know' > example.txt`)
   3. [ ] Exit the container and then jump back in and read the contents of `example.txt` (`cat example.txt`). Note: You need to get the ID of the container you just left with `docker ps -a` in order to run it again with `docker run -it <CONTAINER_ID> /bin/bash`.

If you see the text you've written you've successfully wrote some persistent data - Good job! Remember, docker containers are made in a way so the contents is always the same no matter what machine is running them, by changing something within the container you can successfully override data.

## Stage 3: A bit more hands on work

- [ ] **Done with Stage 3**

Now that we've ran some containers and understood some basics behind them its time to create our own Docker container!

1. [ ] Follow the [Simple application](https://docs.docker.com/get-started/02_our_app/) tutorial and create it.
2. [ ] Continue with this by [Updating the application](https://docs.docker.com/get-started/03_updating_app/).
3. [ ] Learn about [Docker Volumes](https://docs.docker.com/storage/volumes/).
4. [ ] Learn about [Exposing Ports](https://docs.docker.com/engine/reference/commandline/run/#publish)

## Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Contribute valuable responses on at least 5 Docker-related tickets, even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 5: Quiz

- [ ] **Done with Stage 5**

Schedule a call with a [Docker Expert](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-subject.html) (search for `Docker`). During this call, you will guide them through the following:

  1. [ ] Run a `nginx:latest` docker container and expose the container port `80` to `8080` on your local machine (`8080:80`).
  2. [ ] Open a browser and navigate to `localhost:8080` and note the text "Welcome to nginx!"
  3. [ ] While the container is running, jump into the container and change the text "Welcome to nginx!" to "Welcome to Docker!". The file that needs to be edited is located at `/usr/share/nginx/html/index.html`.
  4. [ ] Exit the container, stop it and remove it.
  5. [ ] Create and build the following Dockerfile with the tag of `ubuntu-quiz:latest`:

   ```Dockerfile
   FROM ubuntu:latest
   RUN apt update && apt install vim --no-install-recommends -y
   ```
  6. [ ] Run the image and print where `vim` is installed (`which vim`).
  7. [ ] Remove the image from the system (you need to remove the container first).
  8. [ ] Once you have completed this, have the expert comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage

Congratulations for finishing this module! Final steps:

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
2. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
3. [ ] Consider taking the [Container Registry](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Container%20Registry.md) module or [Continuous Integration](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Continuous%20Integration.md) module.
4. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: docker
     level: 2
   ```

/label ~module
/assign me
/label ~"Module::Docker"
