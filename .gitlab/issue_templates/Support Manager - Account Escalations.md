---
module-name: support-manager-account-escalations
area: Customer Service
maintainers:
  - lyle
---

## Overview

**Goal**: To familiarize Support Managers with the overall process of [Account Escalations](https://about.gitlab.com/handbook/customer-success/csm/escalations/) and the distinct handling steps and tips in the [Support Engineering Manager guide to account escalations](https://handbook.gitlab.com/handbook/support/workflows/escalations-support_manager/).

*Length*: 1hr training + 1 escalation

**Objectives**: At the end of this module, you should be able to:
- Describe the escalation severity levels and identify appropriate DRIs
- Understand how/when to create an account escalation
- Navigate escalations, both escalations where Support is the DRI and where a member of another team is the DRI, from inception to retrospective

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `<module title> - <your name>`
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Join the `#escalated_customers` channel on Slack
1. [ ] Commit to this by notifying the current experts that they can start including you in account escalations as a shadow. 
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: What is an account escalation?

1. [ ] Review the difference between Account Escalations and Support Ticket Attention Requests (STARs): https://about.gitlab.com/handbook/customer-success/csm/escalations/#background
   - [ ] Review ["I want to escalate a ticket"](https://handbook.gitlab.com/handbook/support/internal-support/#i-want-to-escalate-a-ticket) for further clarification about the differences between STARs, Account Escalations, Emergency Tickets, GitLab.com Incidents and Security Incidents (which might all be referred to as "escalation").
1. [ ] Read the descriptions and levels of involvement for each of the [Severity Levels](https://about.gitlab.com/handbook/customer-success/csm/escalations/#background)
   - [ ] Note the [DRIs for each severity level](https://about.gitlab.com/handbook/customer-success/csm/escalations/#escalation-dri)

## Stage 2: Support Manager Roles and Responsibilities

1. [ ] Review [Responsibilities of the Support Manager](https://handbook.gitlab.com/handbook/support/workflows/escalations-support_manager/#responsibilities-of-the-support-manager)
1. [ ] Step through [the workflow for Support Managers working through Account Escalations](https://handbook.gitlab.com/handbook/support/workflows/escalations-support_manager/#workflow)

## Stage 3: Past and current account escalations

1. [ ] Review [past account escalation retrospectives](https://gitlab.com/gitlab-com/support/escalated-customers/-/issues)
   - [ ] Issue link
   - [ ] Issue link

## Stage 4: Shadow on a new account escalation

Notify the `#spt_managers` channel that you are working on this module and ask them to include you as a shadow on an account escalation. 

1. [ ] Shadow two account escalations, where a customer is having trouble.
   1. [ ] Escalation channel link:
   1. [ ] Escalation channel link:

2. [ ] Participate in the [retrospective process](https://handbook.gitlab.com/handbook/support/workflows/escalations-support_manager/#step-6-retrospective)
   1. [ ] Retro link
   2. [ ] Retro link


## Stage 4: Run your own account escalation with an experienced manager shadowing you

Notify the `#spt_managers` channel that you are working on this module and ask to run your own account escalation with someone shadowing you.

1. [ ] Run an account escalations, where a customer is having trouble.
   1. [ ] Escalation channel link:

2. [ ] Participate in the [retrospective process](https://handbook.gitlab.com/handbook/support/workflows/escalations-support_manager/#step-6-retrospective)
   1. [ ] Retro link

3. [ ] Discuss with your shadow how things went and if there were any areas that were missed or might need review/improvement.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer, or someone who has completed this training review your progress.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module
/label ~"Module::Support Manager - Escalations"
/assign me