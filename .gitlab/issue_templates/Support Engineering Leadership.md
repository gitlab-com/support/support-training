---
module-name: "support-engineering-leadership"
area: "Leadership"
maintainers:
  - [rspainhower]
---

### Overview

**Goal**: You understand the leadership programs and opportunities available at GitLab for Support Engineering team members.

**Objectives**: At the end of this module, you should be able to:

- Understand the leadership programs and opportunities available at GitLab.
- Have a career development conversation with your manager including one or more of these leadership programs and opportunities.
- Share information with peers and colleagues about these leadership programs and opportunities.
- Know how to stay up-to-date with new leadership programs and opportunities at GitLab.

### Stage 0: Create Your Module

1. [ ] Create an issue using this template; set the Issue Title: Support Engineering Leadership - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

### Stage 1: Leadership at GitLab - Onboarding Issues

New managers at GitLab complete two significant Onboarding Issues, which cover the basics of GitLab leadership, coaching skills, handling personal information, and other important topics.

* PeopleOps [Becoming A Manager Issue Template](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* Support Engineering [Support Manager Basics Issue Template](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Support%20Manager%20Basics.md)

1. [ ] **If you are currently in an individual contributor role:** Review the Issue Templates to learn more about leadership at GitLab. Work with your manager to identify specific items that could be relevant for your career development.

1. [ ] **If you are currently in a management role:**  There have likely been updates since you completed your manager training. Review the above Issue Templates; create a separate Issue for yourself with any specific items you want to review in-depth.


### Stage 2: Growth and Development Opportunities

The following are growth and development opportunities at GitLab that can help you in current and future roles. Many of these options allow you to develop leadership skills while working as an individual contributor.

**NOTE:** The [Growth and Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) is available for programs that cost money.


#### 1. Buddy Programs

In these programs you serve as an [informal mentor](/handbook/people-group/learning-and-development/mentor/#become-a-mentor) for colleagues at GitLab.

1. [ ] [Onboarding Buddy](/handbook/people-group/general-onboarding/onboarding-buddies/)
1. [ ] [Support Onboarding Buddy](/handbook/support/training/onboarding_buddy.html)
1. [ ] [Merge Request Buddy](/handbook/people-group/general-onboarding/mr-buddies/)
1. [ ] [Parental Leave Reentry Buddy](/handbook/total-rewards/benefits/parental-leave-toolkit/#sts=Parental%20Leave%20Reentry%20Buddies)


#### 2. Developing your Mentoring and Coaching Skills

These external programs help develop your skills as a mentor, coach, or manager.

1. [ ] [Crucial Conversations training](/handbook/people-group/learning-and-development/learning-initiatives/crucial-conversations/)
1. [ ] [Lifelabs Learning Manager training](/handbook/people-group/learning-and-development/learning-initiatives/#lifelabs-learning)


#### 3. Mentoring and Coaching Programs at GitLab

These internal programs either connect you with a mentor, or are opportunities to serve as a mentor to others.

1. [ ] [Handbook page on organized mentorship programs at GitLab](/handbook/people-group/learning-and-development/mentor/#organized-mentorship-programs)
1. [ ] [Become an informal mentor for colleagues](/handbook/people-group/learning-and-development/mentor/#become-a-mentor)
1. [ ] [Merge Request Coach for GitLab Community Members](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html)
1. [ ] [Engineering: Trainee code-maintainer mentorship pilot program](/handbook/engineering/workflow/code-review/#trainee-maintainer-mentorship-pilot-program) (participate as a mentor for a trainee, or participate as a trainee)
1. [ ] [Engineering Internship Program](/handbook/engineering/internships/) (participate as a mentor for an intern)
1. [ ] [Internship For Learning](/handbook/people-group/learning-and-development/career-development/#internship-for-learning) (participate as a mentor for a colleague)
1. [ ] [Volunteer Coach for URGs](/handbook/engineering/volunteer-coaches-for-urgs/)
1. [ ] [Instructor for Advanced Software Engineering Course - Morehouse College](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/advanced-software-engineering-course/)
1. [ ] [Development: Development Director Shadow Experiment](/handbook/engineering/readmes/wayne-haber/development-director-shadow-experiment.html)


#### 4. Mentoring and Coaching Programs outside GitLab

These programs are run by third parties, and are part of your GitLab benefits.

1. [ ] [LinkedIn Learning: How to be a good mentor or mentee](/handbook/people-group/learning-and-development/mentor/#mentor-and-mentee-training)
1. [ ] [Modern Health Coaching](/handbook/total-rewards/benefits/modern-health/) (you are the coachee)
1. [ ] [Plato HQ Mentoring Program](/handbook/engineering/plato/) (you are the mentee)


#### 5. Cross-training With Other GitLab Teams

These programs allow you to shadow or join other GitLab teams temporarily.

1. [ ] [Support Shadow Program](/handbook/support/#support-shadow-program) (be the person that another team member shadows)
1. [ ] [Internship For Learning](/handbook/people-group/learning-and-development/career-development/#internship-for-learning) (participate as an intern)
1. [ ] [Engineering-specific page about the Internship For Learning](/handbook/engineering/career-development/#apprenticeship-for-learning)


#### 6. Manager Training

These programs focus on manager skills and development.

1. [ ] [Development: Aspiring Manager mentorship pilot program](/handbook/engineering/development/dev/training/ic-to-manager/#aspiring-manager-mentorship-pilot-program)
1. [ ] [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/)
1. [ ] [Lifelabs Learning - Manager Core Training](/handbook/people-group/learning-and-development/learning-initiatives/#lifelabs-learning)


#### 7. Interviewing

These programs / modules help you learn how to conduct job interviews at GitLab.

1. [ ] [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
1. [ ] PeopleOps [Interview Training module](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md)
1. [ ] [Support: Assessment Review training](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Support%20Hiring%20-%20Stage1%20Assessment%20review%20training.md)
1. [ ] [Support: Technical Interview training](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Support%20Hiring%20-%20Stage2%20Technical%20Interview%20Training.md)
1. [ ] [Support: advice to candidates about the Technical Interview](/handbook/hiring/interviewing/technical-interview/)


#### 8. Peer Groups

Anyone at GitLab can create, join, or participate in these peer-led groups.

1. [ ] [TMRG (Team Member Resource Group)](https://about.gitlab.com/company/culture/inclusion/erg-guide/)
1. [ ] [Working groups](https://about.gitlab.com/company/team/structure/working-groups/)
1. [ ] [Book clubs](https://gitlab.com/gitlab-com/book-clubs)


#### 9. E-group / director

The following programs are connected to GitLab's executive teams.

1. [ ] [CEO Shadow Program](/handbook/ceo/shadow/)
1. [ ] [Acting Chief of Staff to the CTO](/handbook/engineering/#acting-chief-of-staff-to-the-cto-rotation)


### Stage 3: Add to Your Career Development Plan

1. [ ] Review the [Career Development handbook](/handbook/people-group/learning-and-development/career-development/)
1. [ ] Create a draft [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#individual-growth-plan) (or other suitable format)
1. [ ] Review with your manager in a 1:1 or in a dedicated career development discussion.


### Stage 4: Staying Up to Date

New programs and initiatives are always being piloted, trialed, explored, and offered at GitLab. Ensure you are part of various communication channels to stay up to date on new offerings (and changes to existing offerings):

1. `#whats-happening-at-gitlab`
1. `#company-fyi` (for all GitLab team members and contractors)
1. `#company-fyi-private` (for GitLab team members excluding contractors)
1. `While You Were Iterating` (monthly internal newsletter, emailed to all team members and posted in `#company-fyi`)
1. `#diversity_inclusion_and_belonging`
1. `#learninganddevelopment`
1. `#managers`
1. Slack channels associated with TMRGs


### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask a Support Manager to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).


/label ~module
/label ~"Module::Support Engineering Leadership"
/assign me