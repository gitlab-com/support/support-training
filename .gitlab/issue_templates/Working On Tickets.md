---
module-name: "Working On Tickets"
area: "Customer Service"
maintainers:
  - rspainhower
  - faleksic
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://handbook.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Goals of this checklist**

When you've completed this module you'll have helped your first customers by pairing with Support Engineers and replying to tickets.

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **two weeks to complete**.

### Stage 0. Zendesk Basics

You've completed the Zendesk Basics module. A couple of extra tips that you might find useful:

1. Read about Sensitive information
   - [ ] [Handling Sensitive Information With GitLab Support](https://about.gitlab.com/support/sensitive-information/)
   - [ ] [Removing Information From Tickets](https://handbook.gitlab.com/handbook/support/providing_excellent_customer_service/#removing-information-from-tickets)
1. [ ] Consider installing the [Zendesk Download Router](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router), which will sort downloaded Zendesk ticket attachments into their own folders. This is particularly useful for Solutions focused Support engineers.
1. [ ] Familiarize yourself with the [Support Request for Help workflow](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-use-gitlabcom-to-formally-request-help-from-the-gitlab-development-team), for handling tickets that need involvement from the GitLab Development Group.
1. [ ] Review [the ticketing style guide](https://handbook.gitlab.com/handbook/support/workflows/ticketing_style_guide/) for suggestions that may be helpful for communicating with customers and organizing ticket responses.

### Stage 1. See it all in action!

#### Debugging/troubleshooting tips

1. [ ] Optional. If this is your first go at Support Engineering or Development, talk to your manager about getting a copy of [Pocket Guide to Debugging](https://wizardzines.com/zines/debugging-guide/). Not all sections will be directly relevant, but will help you think about how best to reproduce issues and where to start with tracing bugs in the code.

#### Pairing Sessions

While you can start pairing any time, it is important that you have worked through and finished up until this point so that you can connect what you're learning with what's happening during pairing sessions. As you begin this stage, you should simultaneously be working on the rest of your onboarding including your area(s) of focus.

_Instructions_

- Perform at least **10 pairing sessions**.
- Include people outside of your immediate region to avoid silos.
- Consider pairing more with people from the [support team](https://gitlab-support-readiness.gitlab.io/support-team/) to get to know them better.
- Consider having a pairing/coffee chat with someone you has joined GitLab support around the same time as you to support each other.
- Your first pairing or two are expected to be more of you shadowing. After that, have others help you with your tickets, or grab new tickets.
- Ask questions including how they approach new tickets, what they do when they don't know how to proceed with a ticket, or how they approach [getting help](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-get-help-workflow).
- Create a Google Calendar event, inviting that person. If you're unsure of times due to timezones, feel free to send them a message in Slack first. When it's time for the pairing session, either:
  - Create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing/-/tree/master/.gitlab/issue_templates) and use the appropriate template for the call. You might feel hesitant to create a pairing invite with a person you barely know, but do know that everyone here will be happy to see a pairing/coffee chat calendar invite from you. 
  - Join the [#spt_pairing](https://gitlab.enterprise.slack.com/archives/C03UW0HPBGD) channel and create a new thread where you can both work on issues and tickets together. When you are done, you can use the 
    [Pairify](https://handbook.gitlab.com/handbook/support/workflows/pairify/) tool to automate the creation of the pairing issue in the [Support Pairing](https://gitlab.com/gitlab-com/support/support-pairing/) project.
  - Note: Try to pair with at least 1 person for each: majority SaaS, majority Self-managed, majority L&R [focused](https://gitlab-support-readiness.gitlab.io/support-team/area-of-focus.html). Also include at least 3 seniors, as they can share their expertise and offer guidance on how to grow.
- To "shadow" team members and get a better sense of how support works at GitLab, consider joining group pairing sessions (also called "crush" sessions). You can find them in [**GitLab Support** Google calendar](https://handbook.gitlab.com/handbook/support/#google-calendar).

   1. [ ] Call with your support onboarding buddy; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with your support onboarding buddy; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___

When working tickets, you might need help with particular technology. You can pair with an expert in that topic to help you move a challenging ticket forward. Check out [Skills by Subject](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-subject.html) page to find people you could ask for advice.

When in-depth subject matter expertise is needed and not readily available within the Support Team itself, create a collaboration issue using the [Support Request for Help workflow](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-use-gitlabcom-to-formally-request-help-from-the-gitlab-development-team). A counterpart from the relevant Development Group will be assigned to help investigate and bring the reported issue to a conclusion.

##### Team Tracking

Pairings will be an important part of your day to day work after the onboarding as well. If you're up for a challenge, we also have a **Team Tracking** template that you can use to try and get a pairing session with *every* Support Engineer. Gotta catch them all!

 1. [ ] Add yourself to the [Team Tracking template](https://gitlab.com/gitlab-com/support/support-pairing/-/blob/master/.gitlab/issue_templates/Ticket%20Pairing%20-%20Team%20Tracking.md) so people taking the quest in the future won't miss you!
 1. [ ] Optional: [Instantiate an issue with the template for yourself](https://gitlab.com/gitlab-com/support/support-pairing/-/issues/new?issuable_template=Ticket%20Pairing%20-%20Team%20Tracking) and celebrate your success on Slack once you manage to finish the entire list!

#### Tickets Review

1. [ ] Check out your team's responses on tickets.
   1. [ ] See the tickets in the Global Zendesk instance as they come in and are updated.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses. A recommendation is not to only read through the currently open tickets, but also through the ones that are in Solved status. You can find those by typing `status:solved` in ZenDesk search bar.

## Congratulations! You've now completed the Support Engineer onboarding pathway.

The next step is to discuss with your manager what your first area of focus is. You should then start the pathway for that area (usually "GitLab.com SAAS support" or "Self-managed support").

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Working On Tickets"
