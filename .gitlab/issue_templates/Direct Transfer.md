---
module-name: "Direct Transfer"
area: "Product Knowledge"
gitlab-group: "Foundations: Import & Integrate"
maintainers:
- anton, cms
---
## Overview

GitLab offers multiple ways to import data, from other GitLab instances by way of Direct Transfer and Project Import/Export, as well as having importers for 3rd-party providers, such as GitHub, Bitbucket etc. This module will cover the Direct Transfer method of import. Separate modules will become available for additional import methods.

**Goals of this training module**

After completion of this module, engineers should:

- Understand the **Direct Transfer** import method and appropriate use cases
- Be able to perform and troubleshoot the **Direct Transfer** import method
- Understand authentication requirements for **Direct Transfer**
- Know what data can be migrated using **Direct Transfer**
- Be familiar with the limitations and prerequisites
- Understand how to verify successful imports
- Be familiar with the available API endpoints to manage **Direct Transfer**

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

## Stage 0: Create your module

1. - [ ] Create an issue using this template by making the Issue Title: `Direct Transfer - <your name>`
1. - [ ] Add yourself as the assignee.
1. - [ ] Notify your manager to let them know you've started.
1. - [ ] Commit to this by notifying the current experts that they can start routing non-technical Direct Transfer questions to you.
1. - [ ] Update your [Support Team yaml](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) file to indicate that you've started learning this knowledge area:

   ```yaml
   knowledge_areas:
   - name: Direct Transfer
     level: 1
   ```

1. - [ ] Consider joining the following Slack channels: 
     - [#g_import_and_integrate](https://gitlab.slack.com/archives/C04RDL3MEH5)
     - [#spt_pod_import_and_integrate](https://gitlab.slack.com/archives/C052K0Z1F8T)
1. - [ ] In your Slack Notification Settings, set the following words as a [Keyword Notification](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications):
     - `import`
     - `export`
     - `direct transfer`
     - `dt` (acronym for Direct Transfer)
1. - [ ] *Optional:* Set a milestone, if applicable, and a due date to help motivate yourself!
1. - [ ] *Optional:* Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Understanding Direct Transfer

- [ ] **Done with Stage 1**

1. - [ ] Learn about Direct Transfer fundamentals
   1. - [ ] Read through [Migrating GitLab by using direct transfer](https://docs.gitlab.com/ee/user/group/import/)
   1. - [ ] Understand [what can be migrated](https://docs.gitlab.com/ee/user/group/import/migrated_items.html)
   1. - [ ] Review [prerequisites and limitations](https://docs.gitlab.com/ee/user/group/import/direct_transfer_migrations.html#prerequisites)
      1. - [ ] Note this important point around [Sidekiq configuration](https://docs.gitlab.com/ee/user/project/import/index.html#sidekiq-configuration) and how to optimize imports.
   1. - [ ] Consider [user contribution and membership mapping](https://docs.gitlab.com/ee/user/project/import/index.html#user-contribution-and-membership-mapping) 
   1. - [ ] Study the [troubleshooting guide](https://docs.gitlab.com/ee/user/group/import/troubleshooting.html)
   1. - [ ] Understand the [design decisions](https://docs.gitlab.com/ee/development/bulk_import.html) behind Direct Transfer
   1. - [ ] Read and bookmark the [Direct transfer section in this runbook](https://gitlab.com/gitlab-org/foundations/import-and-integrate/team/-/blob/main/importers/runbook.md#direct-transfer) for some helpful troubleshooting tips.
   1. - [ ] Read and bookmark the [Importer FAQ](https://gitlab.com/gitlab-org/foundations/import-and-integrate/team/-/blob/main/importers/faq.md)
   1. - [ ] Review the [Import and Integrate Group's team page](https://handbook.gitlab.com/handbook/engineering/development/dev/foundations/import-and-integrate) for insights into the engineering team.
   1. - [ ] Bookmark the [GitLab Logs Analysis](https://gitlab.com/gitlab-org/foundations/import-and-integrate/gitlab-logs-analysis) tool that can be used to analyze direct transfer problems within GitLabSOS/KubeSOS tarballs.

## Stage 2: Direct Transfer Practice

- [ ] **Done with Stage 2**

1. - [ ] Set up a suitable GitLab self-managed test instance, ensuring the version is closely aligned with GitLab.com, see [Versions](https://docs.gitlab.com/user/group/import/direct_transfer_migrations/#versions).  This module requires you to perform Direct Transfer (Migration) from GitLab.com to the test instance (and vice versa), providing the opportunity to review both self-managed logs and GitLab.com logs (via Kibana).

     You can perform the first migration in either direction, keeping in mind that as logs on a self-managed instance are generally easier to access, you may prefer to import from GitLab.com to your self-managed instance first. This will help to understand what should be searched in Kibana when troubleshooting import to GitLab.com.
   
   - The test instance must be accessible from GitLab.com’s [IP range](https://docs.gitlab.com/ee/user/gitlab_com/#ip-range), and the instance able to access GitLab.com
   - Ensure that your test instance has data suitable for direct transfer, i.e. a group/subgroup containing a range of GitLab entities, being mindful of the [items migrated when using direct transfer](https://docs.gitlab.com/ee/user/group/import/migrated_items.html#migrated-group-items). Particular attention should also be paid to [user contribution and membership mapping](https://docs.gitlab.com/ee/user/project/import/index.html#user-contribution-and-membership-mapping) as this can be a source of confusion. 
     - When transfering from GitLab.com, you may choose to create a new group specifically for the testing of Direct Transfer, or choose an existing group where you have Owner role. Be mindful of the size of the group and its content.
     - Consider using the [faker gem](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/seed-data-api.md) to seed data into your test instance for transfer to GitLab.com
  
1. - [ ] Perform Direct Transfer migrations
   1. - [ ] Initiate a [group migration](https://docs.gitlab.com/ee/user/group/import/direct_transfer_migrations.html#connect-the-source-gitlab-instance) using the GitLab UI
   1. - [ ] Monitor/review the [migration progress](https://docs.gitlab.com/ee/user/group/import/direct_transfer_migrations.html#review-results-of-the-import)
   1. - [ ] Note suggestions in the [Import FAQ](https://gitlab.com/gitlab-org/foundations/import-and-integrate/team/-/blob/main/importers/faq.md?ref_type=heads#direct-transfer) on log filtering
   1. - [ ] Examine logs for activities performed throughout the migration:
        - On the destination instance
          - Obtain the `correlation_id`  for the initiation of import.
          
            You can check the following:
            - Rails Log
              - UI initiated: `json.meta.called_id: "Import::BulkImportsController#create"` - example [Kibana query](https://log.gprd.gitlab.net/app/r/s/o4UcH).
              - API initiated: `json.meta.caller_id: "POST /api/:version/bulk_imports"` - example [Kibana query](https://log.gprd.gitlab.net/app/r/s/hj9BQ)
  
          - Filter/search for the `correlation_id` or `json.class: "BulkImportWorker"` in Sidekiq logs to observe activities of the Sidekiq jobs, note the classes used:
            - Review the [Sidekiq jobs execution hierarchy](https://docs.gitlab.com/development/bulk_import/#sidekiq-jobs-execution-hierarchy) orchestrated by the [BulkImportWorker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/bulk_import_worker.rb) to identify the related workers, for example:
              - [BulkImports::ExportRequestWorker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/bulk_imports/export_request_worker.rb#L4-4): Manages export requests from the source instance
              - [BulkImports::EntityWorker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/bulk_imports/entity_worker.rb#L4-4): Handles individual entity (group or project) migrations, invoked by [BulkImports::ExportRequestWorker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/bulk_imports/export_request_worker.rb#L4-4)
              - [BulkImports::PipelineWorker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/bulk_imports/pipeline_worker.rb#L4-4): Run for the each step of the data migration for an entity
              - Various other `BulkImports::*` classes will also be observed in logs
        - On the source instance
          - Review `gitlab_access.log` to understand the endpoints accessed by Direct Transfer.
          - Specific workers: `BulkImports::RelationExportWorker`, `BulkImports::RelationBatchExportWorker`, `BulkImports::FinishBatchedRelationExportWorker`
          - Other logs as appropriate depending on any observed errors.
  
1. - [ ] Verify imported data/review import failures
   1. - [ ] Review [this section](https://gitlab.com/gitlab-org/foundations/import-and-integrate/team/-/blob/main/importers/runbook.md?ref_type=heads#debugging-tips-data-missing-after-project-or-group-is-imported) in the [Importer Runbook](https://gitlab.com/gitlab-org/foundations/import-and-integrate/team/-/blob/main/importers/runbook.md) to identify checks that can be performed.
   1. - [ ] Review the [import results](https://docs.gitlab.com/ee/user/group/import/direct_transfer_migrations.html#review-results-of-the-import)
   1. - [ ] Check that expected items have been imported
        - - [ ] Was there any data not imported and is it clear why?
        - - [ ] Check the corresponding repository’s quota/usage page to compare source and destination
          - [ ] *[Optional]:* Consider how you might be able to use the GitLab API and other tools `git clone/branch/count-objects` etc. to perform a comparison of the source and destination repositories. If you create something functional, do share!
   1. - [ ] Check the user mapping to understand how Direct Transfer handles both mapped and unmapped users, see [placeholder users](https://docs.gitlab.com/ee/user/project/import/#placeholder-users)
   1. - [ ] On self-managed imports, review `gitlab-rails/importer.log` (check where this ends up in Kibana)
   1. - [ ] Review Sidekiq logs for any failing jobs or exception messages. [Kibana example](https://log.gprd.gitlab.net/app/r/s/vSuWP)

1. - [ ] *[Optional]*: Practice handling common errors and limitations. Some things to consider:
     - Network related
       - Use firewall rules or network throttling to temporarily block or slow connections between instances
       - Temporarily disable network access midway through a transfer
     - Resource constraints
       - Artificially limit available disk space on the target instance
       - Temporarily reduce RAM availability using cgroups or similar
       - Throttle CPUs
     - Data-related scenarios
       - Create repositories with problematic characters in names or paths
       - Include extremely large files that approach or exceed size limits
     - Authentication/Authorization
       - Temporarily revoke necessary permissions during transfer
       - Use tokens with insufficient scope
     - Process interruption
       - Gracefully/ungracefully stop GitLab services during transfer
       - Restart the target instance mid-migration (obviously not GitLab.com!)
       - Database availability

1. - [ ] Repeat Stage 2 reversing the direction of the import.

## Stage 3: Using the API

- [ ] **Done with Stage 3**

1. - [ ] Familiarize yourself with [Group and project migration by direct transfer API](https://docs.gitlab.com/ee/api/bulk_imports.html)
1. - [ ] Perform a group migration using the appropriate API endpoints to start, monitor and review progress
1. - [ ] Are there any significant differences using the API compared to the GitLab UI?
1. - [ ] Note the API allows you to also [migrate a single project entity](https://docs.gitlab.com/ee/api/bulk_imports.html#start-a-new-group-or-project-migration), which is not possible when using the GitLab-UI.
   1. - [ ] Use the API to migrate a single project
   
## Stage 4: Handling Direct Transfer tickets

- [ ] **Done with Stage 4**

1. - [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have performed troubleshooting in this area.
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __

1. - [ ] Review [Request For Help issues](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=all&label_name%5B%5D=Help%20group%3A%3Aimport%20and%20integrate&search=-%20%5Bx%5D%20~%22Importer%3AGitLab%20Migration%22%20%28aka%20Direct%20Transfer%29%20&first_page_size=100) to find examples where we have requested assistance from the development teams, and also to understand common problems that could be addressed through additional troubleshooting. Note that for **Direct Transfer**, the **Migration Type** `~"Importer:GitLab Migration" (aka Direct Transfer)` should be checked in the issue.

1. - [ ] Note the existence of the [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) tool. Customers may [raise tickets](https://gitlab.zendesk.com/agent/tickets/594590) requesting help with Congregate. This tool is used and maintained by GitLab’s Professional Services team to help perform paid customer migrations. Support for this tool is provided only by way of an [issue](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/issues/new?issuable_template=congregate-support) in the [Congregate repository](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/issues/?sort=created_date&state=opened&first_page_size=100) and customers should be directed there.

1. - [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers. We have troubleshooting pages for most importers in our handbook, feel free to submit a MR to make changes.
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __
   1. - [ ] __

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...
- [ ] Leave your thoughts on this module at the [dedicated feedback issue](https://gitlab.com/gitlab-com/support/support-training/-/issues/3927).

## Final Stage:

1. - [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. - [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. - [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an Importers integrations expert on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module
/label ~"Module::Direct Transfer"
/assign me
