---
module-name: "Relocation to Another Region"
area: "TBD"
maintainers:
  - rspainhower
---

## Overview

**Goal**: Successfully transition from one Support Region to another

This issue tracks tasks related to relocating from one to another regional support team. Issue should be assigned to the relocating team member, and both managers (REGION1 and REGION2).

Most of the steps mentioned below in the issue should be done by you unless specified otherwise.

### 0. PeopleOps updates
For details see [Relocation](https://about.gitlab.com/handbook/people-group/relocation/).
* [ ] [Follow steps to update your employee info for People Group](https://about.gitlab.com/handbook/people-group/relocation/#tasks-for-team-members---relocation-approved), including address and bank information
* [ ] If currently employed by a GitLab PEO: Review [Contract Phase](https://about.gitlab.com/handbook/people-group/relocation/#contract-phase) and [Payroll Process for PEO](https://about.gitlab.com/handbook/finance/payroll/#peo---contractors) in case there are any wind-down steps
* [ ] Review your new location's [Payroll Calendar](https://about.gitlab.com/handbook/finance/payroll/#payroll-cut-off-dates)
* [ ] **If relocating to The Netherlands:** Sign up for Tipalti, the service used for expense reimbursement in the Netherlands -- you get an email link for this (which is misleading, and says "team members don't need to sign up"). **This is necessary for Expensify to reimburse expenses to your bank account.**

### 1. Reporting Structure
* [ ] Update Workday (previous manager must initiate Request, and schedule an effective date)
* [ ] Update [GitLab team page](https://about.gitlab.com/company/team/) -- manager and location information
   * [ ] [basic instructions for team member](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page)
   * [ ] Link to MR: 

### 2. PagerDuty
* [ ] Update phone/SMS number in PagerDuty Profile
* [ ] Open an [Ops PagerDuty Issue](https://gitlab.com/gitlab-com/support/support-ops/other-software/pagerduty/-/issues/): 
   * [ ] Remove from `ROTATION - REGION1` [schedule](https://gitlab.pagerduty.com/schedules#PKPXM8K) (UPDATE LINK TO CORRECT SCHEDULE)
   * [ ] Add to `ROTATION - REGION2` [schedule](https://gitlab.pagerduty.com/schedules#P9SV029) (UPDATE LINK TO CORRECT SCHEDULE)
Link to Issue: 

### 3. Support Team yaml file

Updating your entry here also updates the "Support OOO - REGION" Google Calendars.

Update slugs in your [support team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry):
* [ ] `reports_to` / user tag
* [ ] `region`, `location`, `country`, `tzdata_timezone`
* [ ] `zendesk` --> `groups`
* [ ] `pagerduty` --> `rotations`
Link to MR: 

### 4. Slack

#### Slack Admin -- Access Request Issue

* [ ] Remove from Slack Group `Support Team REGION1`
* [ ] Add to Slack Group `Support Team REGION2`
* [ ] Remove from Slack Group `Support SGG_NAME REGION1`
* [ ] Add to Slack Group `Support SGG_NAME REGION2`
* Link to AR: 

#### Self
* [ ] Leave any REGION1 regional channels
* [ ] Join (or request invitation to) any REGION2 regional channels
* [ ] Update Slack Profile
   * [ ] What I do (IF it contains reference to REGION1)
   * [ ] Phone number
   * [ ] Time zone
   * [ ] Location
   * [ ] Address
   * [ ] Working Hours

### 5. Meetings
* [ ] Remove personal-invitation from REGION1 regional meetings
* [ ] Invite to REGION2 regional meetings
* [ ] Remove personal-invitation from inapplicable REGION1 regional pairings
* [ ] Invite to REGION2 regional pairings

### 6. Calendars

#### Self
* [ ] Update working hours in Google Calendar
* [ ] Update working hours in Clockwise (if using)
* [ ] Update Availability / timezone / working hours in Calendly

#### Time Off by Deel
* [ ] [Update links to Support Time Off calendars](https://handbook.gitlab.com/handbook/support/support-time-off/#support-time-off-calendars)  in Time Off by Deel to reflect SGG group changes

### 7. Gitlab.com
Update [profile](https://gitlab.com/-/profile)
* [ ] Time zone
* [ ] Location
* [ ] Bio

Update your GitLab.com Regional Support Group -- Open an Individual or Bulk Access Request -- assign to support-operations group. 
* [ ] Remove from https://gitlab.com/gitlab-com/support/REGION1
* [ ] Add to https://gitlab.com/gitlab-com/support/REGION2

Link to AR: 

### 8. LinkedIn
* [ ] Update profile
   * [ ] Country/region
   * [ ] Postal code
   * [ ] Locations within this area

### 9. Prelude
* [ ] If you are doing interviews, update your schedule and timezone in [Prelude](https://about.gitlab.com/handbook/hiring/prelude/#prelude).

/label ~module

