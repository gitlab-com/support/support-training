---
module-name: "Jira Integrations"
area: "Product Knowledge"
gitlab-group: "Create:Ecosystem"
maintainers:
  - dwainaina
  - emchang
---

## Overview

**Goal of this checklist:** Set a clear path for Jira integrations expert training

**Objectives:**
Currently, there are two main ways to integrate Jira with GitLab: GitLab Jira integration and Jira Development Panel Integration. This module will cover both methods.

To achieve our overall objective of becoming a GitLab-Jira expert we will need to:
- Able to install Jira Software (Self-Managed) for testing purposes
- Able to set up Jira Cloud for testing purposes
- Able to integrate with Jira Software with GitLab Self-Managed
- Able to integrate with Jira Software with GitLab.com
- Able to integrate with Jira Cloud with GitLab Self-Managed
- Able to integrate with Jira Cloud with GitLab.com
- Able to import Jira issues into GitLab
- Know how to use [Jira Smart Commits](https://confluence.atlassian.com/fisheye/using-smart-commits-960155400.html)

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Jira - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Jira questions to you.
1. [ ] Consider joining the following Slack channels: 
    - [#g_manage_import_and_integrate](https://gitlab.slack.com/archives/C04RDL3MEH5)
    - [#spt_pod_import_and_integrate](https://gitlab.slack.com/archives/C052K0Z1F8T)
1. [ ] In your Slack Notification Settings, set **Jira** as a [Keyword Notification](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications).
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with Jira

- [ ] **Done with Stage 1**

1. [ ] Learn about the Jira integrations available for GitLab
    1. [ ] Read through [Jira integrations](https://docs.gitlab.com/ee/integration/jira/) especially
     the [feature availability](https://docs.gitlab.com/ee/integration/jira/#feature-availability)
    1. [ ] Read through [Configure Jira integration in GitLab](https://docs.gitlab.com/ee/integration/jira/configure.html)
1. [ ] [OPTIONAL] Go through blog posts on Jira posts
    1. [ ] [How to integrate GitLab.com with Jira Cloud](https://about.gitlab.com/blog/2021/03/25/integrating-gitlab-com-with-atlassian-jira-cloud/)
    1. [ ] [How to achieve a GitLab Jira integration](https://about.gitlab.com/blog/2021/04/12/gitlab-jira-integration-selfmanaged/)
    1. [ ] [GitLab and Jira integration: the final steps](https://about.gitlab.com/blog/2021/05/24/gitlab-and-jira-integration-the-final-steps/)
1. [ ] [OPTIONAL] Watch the [recordings of the Jira Training](https://drive.google.com/drive/u/0/folders/1Q8M35fiQ9bEWZudVqpRIhCM5_inX3j_t)

## Stage 2: Setup Jira Server and Cloud

- [ ] **Done with Stage 2**

1. [ ] Create your own Jira Server
    1. [ ] Follow the steps in handbook: [How to Set Up Jira Server](https://handbook.gitlab.com/handbook/support/workflows/jira-server/#how-to-set-up-jira-server)
1. [ ] Set up your own Jira Cloud
    1. [ ] Sign up for an account on [Atlassian](https://id.atlassian.com/signup) if you do not have an account. You can choose `Continue with Google` and login using **your GitLab email.**
    1. [ ] Under **Our Products**, select `Try Jira Software` then click `Get it free` and `Next`.
    1. [ ] Choose a suitable name for your site and click `Agree` and go through the suggested steps

## Stage 3: Integrate with GitLab Jira Integration

- [ ] **Done with Stage 3**

1. [ ] [Create a Jira user in your Jira Server](https://docs.gitlab.com/ee/integration/jira/jira_server_configuration.html). Please use your own email address or any email address that has an account on your GitLab Self-Managed instance and GitLab.com to ensure we will not face any errors in the later steps.
1. [ ] [Generate an API token for your Jira Cloud](https://docs.gitlab.com/integration/jira/configure/#create-a-jira-cloud-api-token).
1. [ ] Configure your Jira Server with your GitLab Self-Managed test instance following the steps in [Configure Jira integration in GitLab](https://docs.gitlab.com/ee/integration/jira/configure.html). You may use the Jira user created in Step 1.
   1. [ ] Get familiar with Jira and GitLab integration:
      1. [ ] Create an issue on your test project on Jira Server.
      1. [ ] Create an issue on your GitLab Project that is connected to Jira.
      1. [ ] Create a new branch on your GitLab Project and make a change to the repository.
      1. [ ] Mention the Jira issue in a commit message on your GitLab Project that is connected to Jira.
      1. [ ] [Close the Jira issue using GitLab](https://docs.gitlab.com/ee/integration/jira/issues.html#close-jira-issues-in-gitlab)
         1. [ ] Create a Merge Request (MR) to [**default** branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html) with the Jira issue in the title, and mention `Closes <JIRA-ID>` in the description.
         1. [ ] Check the title of MR after creation. It should be a hyperlink to the Jira issue directly.
         1. [ ] Check the issue on Jira Server for the commit message posted on GitLab. It should be a comment by the Jira user we set up in Step 1.
         1. [ ] Merge the Merge Request, then check Jira issue - it should be transitioned to `Closed`.
1. [ ] Configure your Jira Cloud with your GitLab Self-Managed test instance following the steps in [Configure Jira integration in GitLab](https://docs.gitlab.com/ee/integration/jira/configure.html). You may use the API token generated in Step 2.
1. [ ] Configure your Jira Server with a new GitLab.com test group following the steps in [Configure Jira integration in GitLab](https://docs.gitlab.com/ee/integration/jira/configure.html). You may use the same Jira user created in Step 1.
1. [ ] Configure your Jira Cloud with a new GitLab.com test group following the steps in [Configure Jira integration in GitLab](https://docs.gitlab.com/ee/integration/jira/configure.html). You may use the same API token generated in Step 2.
1. [ ] [OPTIONAL] [Import Jira issues to GitLab](https://docs.gitlab.com/ee/user/project/import/jira.html#import-jira-issues-to-gitlab). You can watch [Ben King's demo](https://www.youtube.com/watch?v=OTJdJWmODFA).

## Stage 4: Integrate with Jira Development Panel

- [ ] **Done with Stage 4**

1. [ ] [Install GitLab.com for Jira Cloud app](https://docs.gitlab.com/ee/integration/jira/connect-app.html) on your GitLab.com test group.
1. [ ] [Install GitLab for Jira Cloud app on self-managed instance](https://docs.gitlab.com/ee/administration/settings/jira_cloud_app.html#install-the-gitlab-for-jira-cloud-app-manually)

1. [ ] [OPTIONAL] Set up DVCS Account for your Jira Cloud on self-managed instance.  The [GitLab.com for Jira Cloud app](https://docs.gitlab.com/ee/integration/jira/connect-app.html) is preferred because it syncs in real-time but we have the option to set up DVCS account for Jira Cloud.
1. [ ] Set up DVCS Account for Jira Server on either self-managed instance or GitLab.com. **NOTE:** The setup method slightly differs for Jira server 8.14+ and Jira 8.13 earlier.
1. [ ] Use [Smart Commits](https://docs.gitlab.com/ee/integration/jira/dvcs.html#smart-commit-syntax) to transition [Jira issues](https://confluence.atlassian.com/adminjiraserver/working-with-workflows-938847362.html) to [a different status in the Jira Workflow](https://confluence.atlassian.com/adminjiraserver/working-with-workflows-938847362.html). Example: `KEY-123 #comment started working on the issue #in-progress #time 12d 5h.`


## Stage 5: Tickets

- [ ] **Done with Stage 5**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers. We have a [Common Troubleshooting Steps](https://handbook.gitlab.com/handbook/support/workflows/jira-server/#common-troubleshooting-steps-for-jira-tickets) at our handbook, feel free to submit a MR to make changes.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...
- [ ] Leave your thoughts on this module at the [dedicated feedback issue](https://gitlab.com/gitlab-com/support/support-training/-/issues/2079).

## Final Stage:

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as Jira integrations expert on [Skills by Person page](https://gitlab-support-readiness.gitlab.io/support-team/skills-by-person.html).

/label ~module

/label ~"Module::Jira integrations module"
/assign me
