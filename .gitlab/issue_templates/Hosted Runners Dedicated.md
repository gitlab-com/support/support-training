---
module-name: "Hosted runners for GitLab Dedicated"
area: "Customer Service"
maintainers:
  - gerardo
---

# Overview

**Goal**: Set a clear path for GitLab hosted runners for GitLab Dedicated training

**Objectives**: At the end of this module, you should be able to:

- Understand what hosted runners are
- Identify GitLab Dedicated customers using hosted runners
- Understand how to troubleshoot hosted runners for GitLab Dedicated

## General Timeline and Expectations

- This module should take you **1 week** to complete
- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/)

## Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: `<module title> - <your name>`.
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:  

   ```yaml
   knowledge_areas:
   - name: GitLab Dedicated Runners
     level: 1
   ```

Consider using the [Time Tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html) functionality so that the estimated length for the module can be refined.

## Stage 1: Read about hosted Runners on GitLab Dedicated

- [ ] **Done with Stage 1**

1. [ ] Read the introduction section for [hosted runners for GitLab Dedicated](https://docs.gitlab.com/ee/administration/dedicated/hosted_runners.html)
1. [ ] Read the [Security and Network](https://docs.gitlab.com/ee/administration/dedicated/hosted_runners.html#security-and-network) section
1. [ ] Read the [hosted runners for GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/#hosted-runners-for-gitlab-dedicated) section on the GitLab Dedicated product category direction handbook page
1. [ ] Some features are not available to all GitLab offerings (GitLab.com, Self-managed, GitLab Dedicated). Briefly review our [style guide for the product tier badges.](https://docs.gitlab.com/ee/development/documentation/styleguide/availability_details.html)
    1. [ ] View the top of the [hosted runners for GitLab Dedicated](https://docs.gitlab.com/ee/administration/dedicated/hosted_runners.html) page to see how the product tier badges are structured in the documentation. **Note**: If there isn't a tier badge present, it is assumed that the feature in question is available to all offerings.

## Stage 2: GitLab Dedicated Troubleshooting

- [ ] **Done with Stage 2**

**Keep in mind:**

1. Only GitLab Support Engineers and GitLab Dedicated SREs [have access to logs](#searching-logs); GitLab Dedicated customers **do not** have access to real-time logging or the underlying infrastructure. Please do not send any documentation links that are self-managed only and that talk about server-side configuration (check badging at the top of the page or section for **FREE/STARTER/PREMIUM/ULTIMATE ONLY**).
    1. We should not expect customers to have logs for their GitLab Dedicated instance. Customers can [request application logs](https://docs.gitlab.com/ee/administration/dedicated/monitor.html#request-access-to-application-logs) be forwarded to an S3 bucket, but these are not in real-time, and not all customers have requested log forwarding.
1. Neither GitLab Support Engineers nor GitLab Dedicated SREs have access to the administration UI (admin settings pages). Customers have access to their administration UI and are the only people that can administer their GitLab instance.
    1. SREs are unable to access instances, for example the GitLab Rails console, except in the case of emergencies. This procedure is called [breaking glass](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/breaking_glass.md#break-glass-procedure) and has a minimum set of requirements.
1. Troubleshooting problems is the same or similar whether it is on Dedicated, GitLab.com, or Self-managed. The main differences are in where and how we gather the necessary information.

### Identifying customers using hosted runners

1. [ ] With guidance from the [Who is using hosted runners](https://handbook.gitlab.com/handbook/support/workflows/dedicated_runners/#who-is-using-hosted-runners) handbook page, identify two customers using hosted runners. Post a comment stating that you identified two customers successfully, but **do not** name the customers.

### Searching logs

GitLab Dedicated logs are sent to a separate OpenSearch instance for each Dedicated tenant. Support has read-only OpenSearch access, and the credentials are stored in the 1Password vault.

1. [ ] Access OpenSearch by searching in the 1Password `Gitlab Dedicated - Support` vault for the `opensearch` entries and using one of the URLs listed there.
    1. [ ] **Accessing OpenSearch mini exercise**
        1. Select **Global** tenant
        2. Choose **Discover** in the sidebar under **OpenSearch** dashboards
        3. On the next screen, you should see logs. Make sure the index called `gitlab-*` is selected.
    1. Only application logs [can be shared with customers](https://handbook.gitlab.com/handbook/support/workflows/dedicated_logs/#sharing-logs), not the internal Kubernetes logs.
    1. With guidance from the [Viewing Logs](https://handbook.gitlab.com/handbook/support/workflows/dedicated_runners/#viewing-logs) section of the handbook, search for runners logs for one customer and familiarize yourself with the logs.  
1. [ ] Access Grafana by looking in the 1Password `Gitlab Dedicated - Support` vault for the `grafana` entries and using one of the URLs listed there.
    1. Grafana can give you information on the runner status such as possible downtime.
    1. [ ] **Accessing Grafana mini exercise**
        1. Click the four boxes (dashboards) icon
        1. Find the runner dashboard. You can use the [hosted runner monitoring](https://handbook.gitlab.com/handbook/support/workflows/dedicated_runners/#monitoring) handbook section as a guide.
        1. Review the available dashboards.
    1. **Reminder:** Grafana graphs cannot be shared with customers and [are internal use only](https://handbook.gitlab.com/handbook/support/workflows/dedicated/#sharing-internal-logs-data--graphs)

## Stage 3: Getting Help

- [ ] **Done with Stage 3**

**NOTE: The GitLab Dedicated team is currently at its capacity, so their ability to answer Slack requests is best effort.**

- [ ] General questions about GitLab Dedicated can go to the [#support_gitlab-dedicated](https://gitlab.slack.com/archives/C058LM1RL3V) channel. Join this channel so you can ask and answer questions about GitLab Dedicated.

**If you need attention from the GitLab Dedicated team, open an issue in the issue tracker. Do not ping the team or members of the team unless it's an absolute emergency.**

### Filing a Support Request (Request for Help)

These [Customer Support Requests are the highest non-paging priority](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/runbooks/on-call.md#non-emergency-requests) for Dedicated SREs.

- [ ] If an issue can only be resolved through Rails console access, or needs additional troubleshooting from a Dedicated SRE you can [raise a GitLab Dedicated Support Request](https://gitlab.com/gitlab-com/request-for-help/-/issues/new?issuable_template=.gitlab/issue_templates/SupportRequestTemplate-GitLabDedicated). Familiarize yourself with the template so you know what you need before opening a support request.
  - There might be a more specific issue template available; feel free to browse through the available templates for inspiration, but you can always default to the "Support Request" template if unsure.
  - If you pick a more specific issue template, please make sure that the following [quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html) are used:

    ```yaml
    /label "Dedicated::Support Request"
    /confidential
    /label "team::GitLab Dedicated"
    /label "workflow-infra::Ready"
    ```

## Stage 4: Review

- [ ] **Done with Stage 4**

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in [this module's issue template](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/Hosted%20Runners%20Dedicated.md) or in other documentation, list them below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final Stage

1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit an MR to update your `knowledge_areas` slug in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic.

   ```yaml
   knowledge_areas:
   - name: GitLab Dedicated Runners
     level: 2
   ```

/label ~module
/label ~"Module::GitLab Runner Dedicated"
/assign me
