## 1. Training description
This training provides a technical walkthrough demonstrating how to migrate from a single embedded instance of Gitaly to Gitaly Cluster in GitLab

## 2. Prerequisites
Prior to watching the video and reading the document, you will need to create the server nodes that host the GitLab and GitLab Gitaly Clusters. For a walk through on how to create the server resources required for this, please watch this this great [video](https://www.youtube.com/watch?v=aBF-AyQiFfA) from Vlad Stoianovici.

In this training I created the following server resources:

1 GitLab omnibus install
1 Postgres DB
3 praefect nodes
3 Gitaly nodes
1 Load Balancer

## 3. Video link
The configuration of migrating from Omnibus to a Gitaly Cluster environment is detailed in this video [Support Demo - Migrate GitLab from embedded Gitaly to a Gitaly Cluster](https://www.youtube.com/watch?v=eR2qn4QHVzI).

## 4. Training Steps

#### What is Gitaly?

What is Gitaly Cluster and what are its components?
Gitaly provides high-level RPC (remote procedure call) access to Git repositories. It is used by GitLab to read and write Git data.
Gitaly Cluster is an active-active cluster configuration for resilient git operations.
At minimum, this should be:

1 postgresql node (datastore) 
1 pgbouncer (db connection pool, not explicitly required, pictured below, or included in this demo)
3 praefect nodes (coordinators)
3 gitaly nodes (shards)
1 load balancer (I used gcp)


Note that different roles will be applied, so only complete steps 1 and 2 of GitLab installation steps on all the nodes, with the exception of GitLab Application Server which requires all the steps.

Make secrets for all of these and keep them somewhere safe for the time being:
```
GITLAB_SHELL_SECRET_TOKEN
PRAEFECT_EXTERNAL_TOKEN
PRAEFECT_INTERNAL_TOKEN
PRAEFECT_SQL_PASSWORD
PRAEFECT_SQL_PASSWORD_HASH
PGBOUNCER_SQL_PASSWORD_HASH
```

 
#### Gitaly configuration

1 PostgreSQL node
PostgreSQL Setup
These are the only values you want in your gitlab.rb (don’t forget to reconfigure after):
* Disable all components except PostgreSQL
```
roles(['postgres_role'])
prometheus['enable'] = false
alertmanager['enable'] = false
pgbouncer_exporter['enable'] = false
redis_exporter['enable'] = false
gitlab_exporter['enable'] = false
postgresql['listen_address'] = '0.0.0.0'
postgresql['port'] = 5432
```

- Replace the below variable with output of “sudo gitlab-ctl pg-password-md5 gitlab”
`postgresql['sql_user_password'] = '28fb466dbe0f11b85d8cbc7d36802eba'`

- Replace XXX.XXX.XXX.XXX/YY with Network Address
`postgresql['trust_auth_cidr_addresses'] = %w(10.128.20.0/24)`

- Disable automatic database migrations
`gitlab_rails['auto_migrate'] = false`


#### 1 Praefect node (at least, I setup 3)
Database Connectivity Setup

```

CREATE ROLE praefect WITH LOGIN PASSWORD 'abcd1234';

CREATE DATABASE praefect_production WITH OWNER praefect ENCODING UTF8;

ALTER ROLE praefect WITH PASSWORD '28fb466dbe0f11b85d8cbc7d36802eba';

CREATE OR REPLACE FUNCTION public.pg_shadow_lookup(in i_username text, out username text, out password text) RETURNS record AS $$
BEGIN
   SELECT usename, passwd FROM pg_catalog.pg_shadow
   WHERE usename = i_username INTO username, password;
   RETURN;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

REVOKE ALL ON FUNCTION public.pg_shadow_lookup(text) FROM public, praefect;
GRANT EXECUTE ON FUNCTION public.pg_shadow_lookup(text) TO praefect;

```

#### Praefect Setup
These are the only values you want in your gitlab.rb (don’t forget to reconfigure after):
- Configuration

```
praefect['database_host'] = '10.128.20.10'
praefect['database_port'] = 5432
praefect['database_user'] = 'praefect'
praefect['database_password'] = 'abcd1234'
praefect['database_dbname'] = 'praefect_production'

praefect['listen_addr'] = '0.0.0.0:2305'
praefect['prometheus_listen_addr'] = '0.0.0.0:9652'
praefect['separate_database_metrics'] = true
praefect['auth_token'] = 'efgh5678'

praefect['virtual_storages'] = {
 'default' => {
   'nodes' => {
     'gitaly-1' => {
       'address' => 'tcp://10.128.20.7:8075',
       'token'   => 'ijkl9123',
     },
     'gitaly-2' => {
       'address' => 'tcp://10.128.20.8:8075',
       'token'   => 'ijkl9123',
     },
     'gitaly-3' => {
       'address' => 'tcp://10.128.20.6:8075',
       'token'   => 'ijkl9123',
     }
   }
 }
}
```

- Turn all this off
```
gitaly['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
puma['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
gitlab_kas['enable'] = false

# Enable only the Praefect service
praefect['enable'] = true

# Prevent database migrations from running on upgrade automatically
# praefect['auto_migrate'] = false
gitlab_rails['auto_migrate'] = false

# This is the DEPLOY NODE (ONLY ONE NODE SHOULD BE TRUE)
praefect['auto_migrate'] = true
```
 
3 Gitaly nodes
Gitaly Setup:

Disable all other services on the Gitaly node]
```
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
grafana['enable'] = false
puma['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
prometheus_monitoring['enable'] = false

# Enable only the Gitaly service
gitaly['enable'] = true

# Enable Prometheus if needed
prometheus['enable'] = true

# Disable database migrations to prevent database connections during 'gitlab-ctl reconfigure'
gitlab_rails['auto_migrate'] = false

# Make Gitaly accept connections on all network interfaces.
# Use firewalls to restrict access to this address/port.
gitaly['listen_addr'] = '0.0.0.0:8075'

# Enable Prometheus metrics access to Gitaly. You must use firewalls
# to restrict access to this address/port.
gitaly['prometheus_listen_addr'] = '0.0.0.0:9236'

gitaly['auth_token'] = 'ijkl9123'

gitlab_shell['secret_token'] = 'GITLAB_SHELL_SECRET_TOKEN'

gitlab_rails['internal_api_url'] = 'GitLab URL'

git_data_dirs({
 "gitaly-1" => {
   "path" => "/var/opt/gitlab/git-data"
 },
 "gitaly-2" => {
   "path" => "/var/opt/gitlab/git-data"
 },
 "gitaly-3" => {
   "path" => "/var/opt/gitlab/git-data"
 
})

 ```
1 Load balanacer
First create an instance group with your praefect nodes:

Then add tcp/2305 to the firewall:

Then you can add the praefect instance group to a new TCP load balancer:

With the health check looking at port 2305 of each node:

 
Migrate GitLab From Embedded Gitaly to Gitaly Cluster
1 seeded GitLab Application server
Add these changes to the gitlab.rb:

```
external_url 'http://omnibus-instance.gyoachum-3k.gcp.gitlabsandbox.net'

gitaly['enable'] = false
git_data_dirs({
 "default" => {
   "gitaly_address" => "tcp://10.128.20.14:2305",
   "gitaly_token" => 'efgh5678'
 }
})
gitlab_shell['secret_token'] = 'zyxw4321'
prometheus['scrape_configs'] = [
 {
   'job_name' => 'praefect',
   'static_configs' => [
     'targets' => [
       '10.128.20.5:9652', # praefect-1
       '10.128.20.2:9652', # praefect-2
       '10.128.20.4:9652', # praefect-3
     ]
   ]
 },
 {
   'job_name' => 'praefect-gitaly',
   'static_configs' => [
     'targets' => [
       '10.128.20.7:9236', # gitaly-1
       '10.128.20.8:9236', # gitaly-2
       '10.128.20.6:9236', # gitaly-3
     ]
   ]
 }
]
```


Remember to Run the command: `gitlab-ctl reconfigure`


## 5. Troubleshooting Steps
Troubleshooting in-call:

After reconfiguring the gitlab application to disable local Gitaly and point to Gitaly Cluster, the UI began throwing 500 errors. 

I ran the following to look for 500 errors in the logs: `grep -HRin '"status":500' /var/log/gitlab/ `


And we saw the error:
```
/var/log/gitlab/gitlab-rails/exceptions_json.log:51:{"severity":"ERROR","time":"2022-06-22T08:33:18.322Z","correlation_id":"01G658NFPCXHJXXS3DCPQM7YPV","exception.class":"GRPC::Unavailable","exception.message":"14:failed to connect to all addresses.
```

Running this command from the GitLab application node confirmed that we were not connecting to Gitaly. 

```
/opt/gitlab/embedded/bin/gitaly-hooks check /var/opt/gitlab/gitaly/config.toml
```

We validated that health of the praefect nodes with:
```
root@praefect-1:~# sudo /opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml dial-nodes
[tcp://10.128.20.6:8075]: dialing...
[tcp://10.128.20.7:8075]: dialing...
[tcp://10.128.20.8:8075]: dialing...
[tcp://10.128.20.6:8075]: dialed successfully!
[tcp://10.128.20.6:8075]: checking health...
[tcp://10.128.20.7:8075]: dialed successfully!
[tcp://10.128.20.7:8075]: checking health...
[tcp://10.128.20.8:8075]: dialed successfully!
[tcp://10.128.20.8:8075]: checking health...
[tcp://10.128.20.6:8075]: SUCCESS: node is healthy!
[tcp://10.128.20.6:8075]: checking consistency...
[tcp://10.128.20.8:8075]: SUCCESS: node is healthy!
[tcp://10.128.20.8:8075]: checking consistency...
[tcp://10.128.20.7:8075]: SUCCESS: node is healthy!
[tcp://10.128.20.7:8075]: checking consistency...
[tcp://10.128.20.6:8075]: SUCCESS: confirmed Gitaly storage "gitaly-3" in virtual storages [default] is served
[tcp://10.128.20.6:8075]: SUCCESS: node configuration is consistent!
[tcp://10.128.20.8:8075]: SUCCESS: confirmed Gitaly storage "gitaly-2" in virtual storages [default] is served
[tcp://10.128.20.8:8075]: SUCCESS: node configuration is consistent!
[tcp://10.128.20.7:8075]: SUCCESS: confirmed Gitaly storage "gitaly-1" in virtual storages [default] is served
[tcp://10.128.20.7:8075]: SUCCESS: node configuration is consistent!
```

And we validated gitaly nodes with:


```
root@gitaly-1:/var/opt/gitlab/git-data/repositories/@hashed# /opt/gitlab/embedded/bin/gitaly-hooks check /var/opt/gitlab/gitaly/config.toml
Checking GitLab API access: OK
GitLab version: 15.0.3-ee
GitLab revision:
GitLab Api version: v4
Redis reachable for GitLab: true
OK
```

This more or less left the load balancer, as the main cause of the issue. 

Upon further inspection, this issue was the load balancer configuration and this is because I initially set up the load balancer to connect directly to gitaly rather than the praefect nodes, which should be handling all the connections for gitaly. I have amended the screenshots, in the load balancer above, to reflect the correct configuration.



