# Strace Quiz - tracing for fun and profit

The following is a series of exercises using `strace` output from real production incidents with a variety of problems. The goal is to give you a gentle introduction to understanding syscalls and how they relate to the applications we use at GitLab.

We'll use the [`strace parser`](https://gitlab.com/gitlab-com/support/toolbox/strace-parser) to quickly analyze the files and find a place to start our investigations.  You can download the latest binaries of the parser at https://gitlab.com/gitlab-com/support/toolbox/strace-parser/-/releases.

### Process

A good place to start with a performance issue is to ask three questions:

   1. Are there syscalls that are taking an unusually long time to execute?
   1. Are there syscalls being called in unusual quantities?
   1. If 1 or 2, why?

Start by running the parser on the file, then `grep` or `less` through it to find specific information on a call. Once you gotten a sense for file, the `answer` section gives a step-by-step guide to finding and interpreting the key details.

You may never have heard of some of the syscalls `strace` lists, that's OK!  [man7.org](http://man7.org/linux/man-pages/man2/syscalls.2.html) has excellent documentation on everything we look at here, and in general we're looking at simple file operations like [`read`](http://man7.org/linux/man-pages/man2/read.2.html), [`write`](http://man7.org/linux/man-pages/man2/write.2.html), [`open`](http://man7.org/linux/man-pages/man2/open.2.html), and [`close`](http://man7.org/linux/man-pages/man2/close.2.html).

**Note**: The below files are based on traces from actual customer tickets. To protect any sensitive data the output has been sanitized - your file names and paths will be slightly different.

### Question 1
Before starting the quiz download the `strace-parser`, then download the file below.

**File**: https://drive.google.com/open?id=1-XPzSSQwvaVcpCYpSuISa77IGzlFoRoz

**Scenario**: A customer is experiencing poor performance across their instance, they have found high CPU usage and Unicorn workers being reaped rapidly.  Our initial investigation finds that they only have three Unicorn worker processes on an eight core machine, and we have the customer bump their worker count to CPU count + 1 and increase the min and max RSS for Unicorn.

These steps moderate the issue, but do not resolve it.  Searching for stuck processes using `ps aux | awk '$8 ~ /D/ { print $0 }'`, we see 20-30 `git` at a time in `D` state.  Processes in `D` state (also known as uninterruptible sleep mode) either gets the timeout value before going to sleep or waits for a particular resource (such as I/O) to become available.

The processes are not stuck indefinitely, they do eventually finish, but new processes are consistently being created, keeping the number fairly stable.  The customer's git repository data is housed on an NFS mount.

At this point we traced a `git` processes with `ps aux | awk '$8 ~ /D/ { print $0 }' | head -1 | awk '{print " -p " $2}' | xargs strace -fttTyyy -s 1024 -o /tmp/warmup.txt`, the output of which you just downloaded.

**Questions**:

   * Are any syscalls running slowly?

<details>
<summary>Answer</summary>

Let's start by running the basic `strace-parser` query on the file:

```
$ strace-parser question1.txt summary

Top 1 PIDs by Active Time
-----------

  pid    	 actv (ms)	 wait (ms)	total (ms)	% of actv	 syscalls	 children
  -------	----------	----------	----------	---------	---------	---------
  100016 	 41487.492	     0.000	 41487.492	  100.00%	      612	        0

Total PIDs: 1
System Time: 41.487492s
Real Time: 43.43760s
```

We see one PID which spent 41 seconds alive (`total (ms)`) during the trace.  All of this this time was spent actively performing tasks (`actv (ms)`).  Let's see the details of the PID using the `--pid / -p` flag.

```
$ strace-parser question1.txt pid 100016

PID 100016
612 syscalls, active time: 41487.492ms, total time: 41487.492ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  open           	      51	 22144.809	   720.865	   434.212	   365.914	ENOENT: 1
  close          	      53	 11766.886	   581.073	   222.017	     0.010
  munmap         	      18	  7450.620	   507.645	   413.923	     0.069
  stat           	      28	    38.238	     4.265	     1.366	     1.041
  lstat          	      45	    35.910	     5.261	     0.798	     0.035
  write          	     275	    18.630	     0.411	     0.068	     0.011
  read           	      67	    14.171	    11.049	     0.212	     0.011
  openat         	       3	     7.336	     3.341	     2.445	     1.381	ENOENT: 1
  getdents       	       5	     4.519	     3.231	     0.904	     0.035
  mmap           	      23	     3.090	     1.911	     0.134	     0.021
  fstat          	      25	     1.820	     0.491	     0.073	     0.016
  fcntl          	      10	     0.681	     0.215	     0.068	     0.037
  brk            	       2	     0.428	     0.378	     0.214	     0.050
  lseek          	       5	     0.274	     0.074	     0.055	     0.038
  alarm          	       1	     0.040	     0.040	     0.040	     0.040
  getrlimit      	       1	     0.037	     0.037	     0.037	     0.037
  exit_group     	       0	     0.000	     0.000	     0.000	     0.000
  ---------------

  Slowest file open times for PID 100016:

   open (ms)	    timestamp    	        error     	   file name
  ----------	-----------------	   ---------------	   ---------
     720.865	 14:23:04.341284 	          -       	   ./refs/tags/TAG01
     654.426	 14:23:05.539496 	          -       	   ./refs/tags/TAG02
     537.596	 14:23:19.768908 	          -       	   ./objects/bb/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b421
     480.545	 14:23:16.608541 	          -       	   ./objects/pack/pack-0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b427d6.pack
     463.103	 14:23:11.212248 	          -       	   ./objects/12/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b422
     461.731	 14:23:07.547567 	          -       	   ./refs/tags/TAG03
     460.785	 14:23:17.510702 	          -       	   ./objects/pack/pack-0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b427d6.pack
     459.996	 14:23:21.664412 	          -       	   ./objects/9c/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b423
     457.537	 14:23:17.984263 	          -       	   ./objects/34/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b424
     456.478	 14:23:13.025965 	          -       	   ./objects/8c/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b425
```

So what stands out here?

`open` calls are taking almost half a second on average, and up to nearly three quarters of a second.  The slowest call is a third of a second.  Typical times for these calls are 10 - 20ms on NFS, under a ms on a SSD.

`close` and `munmap` calls are similarly slow, with the exception that at least one call ran quickly. Again, this is MUCH slower than normal

`read` and `write` are working normally, despite the problems with `open` and `close`.


It seems that the app server is having trouble trouble obtaining and releasing file handles on the NFS mount, but works normally once the file is ready to read/write.

Let's take a look at the distribution of `close` duration using the `quantize` subcommand:

```
$ strace-parser question1.txt quantize close -p 100016

  syscall: close
  pids: 100016

    μsecs       	     count	 distribution
    ------------	  --------	 ----------------------------------------
       8 -> 15  	         3	|▇▇▇▇                                    |
      16 -> 31  	         0	|                                        |
      32 -> 63  	        18	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇              |
      64 -> 127 	         4	|▇▇▇▇▇                                   |
     128 -> 255 	         0	|                                        |
     256 -> 511 	         0	|                                        |
     512 -> 1K  	         0	|                                        |
      1K -> 2K  	         0	|                                        |
      2K -> 4K  	         0	|                                        |
      4K -> 8K  	         0	|                                        |
      8K -> 16K 	         0	|                                        |
     16K -> 32K 	         0	|                                        |
     32K -> 64K 	         0	|                                        |
     64K -> 128K	         0	|                                        |
    128K -> 256K	         0	|                                        |
    256K -> 512K	        27	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇|
    512K -> 1M  	         1	|▇                                       |
```

Note that the times on this chart are in microseconds.  We see a pretty even split between normal (up to a few milliseconds) and slow calls.

</details>

***

 * What is causing these syscalls to take so long?

<details>
<summary>Answer</summary>

Let's take a closer look at these slow calls:

```
$ grep "^100016.*close.*>$" question1.txt

100016 14:22:47.801465 close(4</nfs/git-data/repositories/group/project.git/refs/tags/TAG01>) = 0 <0.405378>
100016 14:22:48.596801 close(4</nfs/git-data/repositories/group/project.git/refs/tags/TAG02>) = 0 <0.407362>
<0.398248>
...
100016 14:23:26.356183 close(3</nfs/git-data/repositories/group/project.git/objects/e0/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b425>) = 0 <0.000010>
100016 14:23:27.217527 close(3</nfs/git-data/repositories/group/project.git/objects/a7/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b426>) = 0 <0.000037>
100016 14:23:28.069338 close(3</nfs/git-data/repositories/group/project.git/objects/a7/0267ec0a78fdc24b1d1b9a3e4dcba3e0f3b427>) = 0 <0.000035>
```

So tags are much slower to close than objects, but not to open, odd.  I don't have an explanation for why only some `close` calls would be impacted, but we had enough information to advise the customer to take a look at their NFS mount.

They found that their NFS mount had been misconfigured; once they corrected the settings the issue was resolved.

</details>

***

#### Question 2

**File**: https://drive.google.com/open?id=11KpvJdsFlr9bDn38hWDlozDihxiOyqR1

**Scenario**: Since upgrading their GitLab instance to version 10.8 customers found that viewing their list of project was extremely slow, to the point of intermittent timeouts.  Logs show that the DB calls are running quickly, the slowness is occurring when building the view, and the instance appears to be in good health.  After some configuration cleanup loading the projects page was down to 15-20 seconds, better, but still unacceptably slow.

To attempt to narrow down the cause of the problem, we had the customer sign into `gitlab-rails c`, start an `strace` on the console session PID, then replicate a request to the slow page with `app.get('/dashboard/projects?on_archived=true&page=4&=latest_activity&private_token=12334')`.

**Questions**:

   * Are any syscalls running slowly?

<details>
<summary>Answer</summary>

First let's get a high-level summary of the trace:

```
$ strace-parser question2.txt summary

Top 7 PIDs by Active Time
-----------

  pid    	 actv (ms)	 wait (ms)	total (ms)	% of actv	 syscalls	 children
  -------	----------	----------	----------	---------	---------	---------
  16295  	 14735.156	   165.639	 14900.795	   99.91%	    40607	        0
  16732  	    10.869	 18403.023	 18413.893	    0.07%	      335	        0
  16730  	     1.525	 24535.453	 24536.979	    0.01%	       61	        0
  16733  	     1.408	 26622.746	 26624.154	    0.01%	      127	        0
  16729  	     0.072	 17761.164	 17761.236	    0.00%	        3	        0
  16731  	     0.000	     0.000	     0.000	    0.00%	        0	        0
  16300  	     0.000	     0.000	     0.000	    0.00%	        0	        0

Total PIDs: 7
System Time: 102.237061s
Real Time: 26.26634s
```

Looks like only PID 16295 has any real active time, the rest are either short-lived or sleeping most of the time.

```
$ strace-parser question2.txt pid 16295

PID 16295
40607 syscalls, active time: 14735.157ms, total time: 14900.796ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  open           	    6603	  8782.005	    19.755	     1.330	     0.029
  read           	     647	  4141.759	  4119.229	     6.401	     0.013	EAGAIN: 1
  getdents       	   12579	   629.357	    19.222	     0.050	     0.011
  lstat          	    6367	   576.752	    35.406	     0.091	     0.014
  stat           	    5896	   285.697	    10.670	     0.048	     0.014	ENOENT: 217
  close          	    6607	   212.513	     0.528	     0.032	     0.012
  select         	      54	   113.842	    18.441	     2.108	     0.014
  futex          	      97	    40.572	     5.081	     0.418	     0.012	EAGAIN: 6
  sendto         	      43	    27.860	     1.842	     0.648	     0.017
  write          	     447	    20.785	     0.454	     0.046	     0.017	EPIPE: 4
  clock_gettime  	     463	    18.717	     0.520	     0.040	     0.010
  access         	     120	    17.433	     3.055	     0.145	     0.014	ENOENT: 60
  epoll_wait     	      39	    11.225	     3.537	     0.288	     0.013
  fcntl          	     432	    11.184	     0.299	     0.026	     0.012
  recvfrom       	     139	     6.298	     1.643	     0.045	     0.012	EAGAIN: 64
  sendmsg        	      23	     2.675	     0.561	     0.116	     0.027
  fstat          	      24	     0.741	     0.059	     0.031	     0.015
  recvmsg        	      10	     0.652	     0.195	     0.065	     0.014	EAGAIN: 5
  connect        	       4	     0.341	     0.112	     0.085	     0.048
  socket         	       4	     0.133	     0.044	     0.033	     0.024
  ioctl          	       3	     0.118	     0.050	     0.039	     0.033
  rt_sigreturn   	       4	     0.091	     0.034	     0.023	     0.012	EPIPE: 4
  rt_sigaction   	       2	     0.046	     0.030	     0.023	     0.016
  ---            	       0	     0.000	     0.000	     0.000	     0.000
  ---------------

  Slowest file open times for PID 16295:

   open (ms)	    timestamp    	        error     	   file name
  ----------	-----------------	   ---------------	   ---------
      19.755	 13:52:43.412959 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189ee
      18.690	 13:52:44.324750 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmpv/37a5084c4cd64763bb66e179a55189e
      18.584	 13:52:46.478254 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f1
      16.648	 13:52:38.386650 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f2
      15.948	 13:52:45.979006 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f3
      14.285	 13:52:38.436089 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f4
      14.093	 13:52:39.907778 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f5
      14.085	 13:52:46.665836 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f6
      14.067	 13:52:39.852646 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f7
      13.973	 13:52:48.761464 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f8
```

The max time for `read` is high, but its average is not.  Let's take a look at how its durations are distributed:

```
$ strace-parser question2.txt quantize read -p 16295

  syscall: read
  pids: 16295

    μsecs       	     count	 distribution
    ------------	  --------	 ----------------------------------------
       8 -> 15  	       129	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇                    |
      16 -> 31  	       234	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇   |
      32 -> 63  	       248	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇|
      64 -> 127 	        26	|▇▇▇▇                                    |
     128 -> 255 	         6	|                                        |
     256 -> 511 	         2	|                                        |
     512 -> 1K  	         1	|                                        |
      1K -> 2K  	         0	|                                        |
      2K -> 4K  	         0	|                                        |
      4K -> 8K  	         0	|                                        |
      8K -> 16K 	         0	|                                        |
     16K -> 32K 	         0	|                                        |
     32K -> 64K 	         0	|                                        |
     64K -> 128K	         0	|                                        |
    128K -> 256K	         0	|                                        |
    256K -> 512K	         0	|                                        |
    512K -> 1M  	         0	|                                        |
      1M -> 2M  	         0	|                                        |
      2M -> 4M  	         1	|                                        |
```

The vast majority of calls were fast with the exception of one sloooooooow outlier .  Let's see what's going on there by `grep`ing for the only read call that took four seconds:

```
$ grep -n "^16295.*read.*<4" question2.txt
23:16295 13:52:36.786530 <... read resumed> "a", 1) = 1 <4.119229>
```

Hmm, so we spend 4 seconds to read the letter 'a', that seems strange.  Let's take a look at the context of the call

```
$ awk 'NR>=20 && NR <=38 {print NR ": " $0}' question2.txt
20: 16733 13:52:36.278143 futex(0x7fac2aa904e0, FUTEX_WAKE_PRIVATE, 1) = 0 <0.000015>
21: 16733 13:52:36.278210 clock_gettime(CLOCK_MONOTONIC, {2018032, 679266513}) = 0 <0.000012>
22: 16733 13:52:36.278282 futex(0x7fac2aa904a4, FUTEX_WAIT_BITSET_PRIVATE, 5638, {2018033, 607967327}, ffffffff <unfinished ...>
23: 16295 13:52:36.786530 <... read resumed> "a", 1) = 1 <4.119229>
24: 16295 13:52:36.787548 write(1</dev/pts/5>, "a", 1) = 1 <0.000040>
25: 16295 13:52:36.787692 read(0</dev/pts/5>, "p", 1) = 1 <0.000016>
26: 16295 13:52:36.788075 write(1</dev/pts/5>, "p", 1) = 1 <0.000018>
27: 16295 13:52:36.788160 read(0</dev/pts/5>, "p", 1) = 1 <0.000016>
28: 16295 13:52:36.788534 write(1</dev/pts/5>, "p", 1) = 1 <0.000017>
29: 16295 13:52:36.788617 read(0</dev/pts/5>, ".", 1) = 1 <0.000015>
30: 16295 13:52:36.789015 write(1</dev/pts/5>, ".", 1) = 1 <0.000017>
31: 16295 13:52:36.789097 read(0</dev/pts/5>, "g", 1) = 1 <0.000015>
32: 16295 13:52:36.789532 write(1</dev/pts/5>, "g", 1) = 1 <0.000017>
33: 16295 13:52:36.789616 read(0</dev/pts/5>, "e", 1) = 1 <0.000015>
34: 16295 13:52:36.790065 write(1</dev/pts/5>, "e", 1) = 1 <0.000018>
35: 16295 13:52:36.790253 read(0</dev/pts/5>, "t", 1) = 1 <0.000023>
36: 16295 13:52:36.790728 write(1</dev/pts/5>, "t", 1) = 1 <0.000017>
37: 16295 13:52:36.790812 read(0</dev/pts/5>, "(", 1) = 1 <0.000015>
38: 16295 13:52:36.791374 write(1</dev/pts/5>, "(", 1) = 1 <0.000018>
```

Lines 20-22 are from a different PID and can be ignored; it looks like the `read` call was polling the rails console and awaiting user input.  So this was the delay between when we started the strace and the customer typed his first character, not a problem after all.

</details>

***

   * Do any syscalls have an unusually high call count?

<details>
<summary>Answer</summary>

Let's look at the summary of PID 16295 again:

```
$ strace-parser question2.txt summary

PID 16295
40607 syscalls, active time: 14735.157ms, total time: 14900.796ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  open           	    6603	  8782.005	    19.755	     1.330	     0.029
  read           	     647	  4141.759	  4119.229	     6.401	     0.013	EAGAIN: 1
  getdents       	   12579	   629.357	    19.222	     0.050	     0.011
  lstat          	    6367	   576.752	    35.406	     0.091	     0.014
  stat           	    5896	   285.697	    10.670	     0.048	     0.014	ENOENT: 217
  close          	    6607	   212.513	     0.528	     0.032	     0.012
  ...
```

The combined calls for `read` and `open` account for the majority of our active time, but we can disregard `read` as virtually all of our time on that is spent waiting for the user to start typing.  `getdents` and `stat`/`lstat` account for most of the remainder of the time.

So our process is being dominated by the cost of opening 12,579 directories (`getdents`) and checking them  (`open` and `stat`).  The files are probably not very large, though, given how little time we spend on `read`.  This sounds a lot like iterating through unpacked git refs!

</details>

***

   * What is causing these syscalls to take so long?

<details>
<summary>Answer</summary>

We know that opening files and directories appear to be cause of the slowness.  We can check which files were opened using the `--files / -f` flag:

```
$ strace-parser question2.txt files -p 16295

Files Opened

      pid	 open (ms)  	    timestamp    	        error     	   file name
  -------	------------	-----------------	   ---------------	   ---------
    16295	       1.096	 13:52:37.621754 	          -       	   /nfs/git-data/repositories/group/project.git/config
    16295	       0.076	 13:52:37.623612 	          -       	   /var/opt/gitlab/.gitconfig
    ... skip 6000 or so lines
    16295	       0.803	 13:52:51.836994 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f1
    16295	       1.067	 13:52:51.838232 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f2
    16295	       0.866	 13:52:51.839708 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f3
    16295	       1.037	 13:52:51.840991 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f4
    16295	       0.809	 13:52:51.842535 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f5
    16295	       0.883	 13:52:51.843777 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f6
    16295	       0.922	 13:52:51.845075 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f7
    16295	       0.782	 13:52:51.846433 	          -       	   /nfs/git-data/repositories/group/project.git/refs/tmp/37a5084c4cd64763bb66e179a55189f8
    ...
```

A quick scan shows a lot of these `refs/tmp/` files, how many?

```
$ strace-parser question2.txt files -p 16295| grep 'refs/tmp/' | wc -l
  5713
```

That's a lot of unpacked files to be floating around.  After confirming with Development, we had the customer delete the `tmp/refs/` files.

After the cleanup the page was loading in 4-5 seconds (no Redis cache) we took another `strace` to compare.

**Post-cleanup strace file**: https://drive.google.com/open?id=1PZCueNaVpdGfI5CrV9nDcZ9L_Qn58jtu

Taking a quick look at the `open` count, we went from 6000+ files being opened to 875, more than halving the time to load the page.

```

PID 16295
11889 syscalls, active time: 8412.299ms, total time: 8826.911ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  read           	     661	  4120.246	  4095.016	     6.233	     0.013	EAGAIN: 5
  open           	     875	  2649.331	    36.782	     3.028	     0.029
  lstat          	     639	   876.784	    31.400	     1.372	     0.016
  stat           	    5900	   435.858	    13.764	     0.074	     0.014	ENOENT: 217
  select         	      64	   279.386	    57.298	     4.365	     0.021
  getdents       	    1118	   186.460	    14.750	     0.167	     0.012
  ...
```

The customer was satisfied with this performance and we wrapped up.

</details>

***

#### Question 3

**File**: https://drive.google.com/open?id=1cVJBogkFN_iRwY3zUFU7qAbGGxII3tXj

**Scenario**: A customer is testing out GitLab 11.3.4 in their pre-prod environment and seeing odd hangs and timeouts on large pushes.  The the SSH transfer will complete normally, the problem occurs after the server has received the files.

```
$ git push origin master 
Counting objects: 65007, done. 
Delta compression using up to 2 threads. 
Compressing objects: 100% (16543/16543), done. 
Writing objects: 100% (65007/65007), 117.34 MiB | 5.88 MiB/s, done. 
Total 65007 (delta 46690), reused 65004 (delta 46689) 
remote: Resolving deltas: 100% (46690/46690), done. 
remote: GitLab: API is not accessible 
 
! [remote rejected] master -> master (pre-receive hook declined) 
error: failed to push some refs to 'git@gitlab.customer.com'
```

Gitaly is logging the following error:

```
2018-12-05_18:30:53.13130 time="2018-12-05T18:30:53Z" level=info msg="finished streaming call" error="rpc error: code = Canceled desc = rpc error: code = Canceled desc = context canceled" grpc.code=Canceled grpc.meta.auth_version=v1 grpc.meta.client_name=gitlab-web grpc.method=GetNewLFSPointers grpc.request.glRepository=project-1 grpc.request.repoPath=group/projects.git grpc.request.repoStorage=default grpc.request.topLevelGroup=core-build grpc.service=gitaly.BlobService grpc.time_ms=30000 peer.address=@ span.kind=server system=grpc
```

I was able to reproduce the behavior on my own GitLab instance with 11.3.4, but not on 11.5.0 or higher.  I ran `strace` on gitaly with `ps auwx | grep gitaly | awk '{ print " -p " $2}' | xargs strace -ttTfyyy -s 1024 -o /tmp/question3.txt` to dig in further.

**Questions**:

   * Are any syscalls running slowly?

<details>
<summary>Answer</summary>

This one is more complex than the other questions because we're tracing all of the gitaly processes, and gitaly itself will fork lots of processes as well.  Let's start as always with taking a look at the summary of the `strace` session:

```
$ strace-parser question3.txt summary

Top 25 PIDs by Active Time
-----------

  pid    	 actv (ms)	 wait (ms)	total (ms)	% of actv	 syscalls	 children
  -------	----------	----------	----------	---------	---------	---------
  19130  	 20840.652	     0.761	 20841.414	   27.45%	       12	        0
  19128  	 20798.748	     0.000	 20798.748	   27.40%	      730	        0
  17487  	  9141.278	 11551.521	 20692.801	   12.04%	   132729	        6
  19094  	  8838.615	 22496.893	 31335.508	   11.64%	      502	       10
  19095  	  4259.111	  4462.025	  8721.136	    5.61%	      350	        0
  19097  	  3425.759	     0.000	  3425.759	    4.51%	    55096	        0
  19376  	   760.759	     0.106	   760.865	    1.00%	    13895	        2
  17378  	   656.497	 37174.090	 37830.586	    0.86%	     9382	       15
  17575  	   607.117	 37938.879	 38545.996	    0.80%	     2414	       25
  17574  	   570.016	 36742.012	 37312.027	    0.75%	     2428	       31
  17479  	   417.523	 38076.684	 38494.207	    0.55%	    11109	        0
  17493  	   378.949	 37315.406	 37694.355	    0.50%	     5563	        0
  17397  	   373.626	 35901.363	 36274.988	    0.49%	     3467	       16
  19122  	   282.722	 20994.383	 21277.105	    0.37%	     3741	        3
  17516  	   254.890	 38519.695	 38774.586	    0.34%	    10959	        0
  19188  	   252.840	    20.900	   273.740	    0.33%	     3775	        2
  17382  	   251.664	 35452.680	 35704.344	    0.33%	     5722	        0
  19272  	   213.196	     0.000	   213.196	    0.28%	      256	        0
  19275  	   163.996	     0.000	   163.996	    0.22%	      254	        0
  19136  	   144.743	     0.000	   144.743	    0.19%	     2027	        0
  19271  	   144.079	     0.000	   144.079	    0.19%	      254	        0
  19135  	   138.769	     0.000	   138.769	    0.18%	     2033	        0
  19276  	   136.481	     0.000	   136.481	    0.18%	      257	        0
  17490  	   131.849	 33623.410	 33755.258	    0.17%	     5834	        3
  19277  	   107.136	     0.000	   107.136	    0.14%	      257	        0

Total PIDs: 210
System Time: 1350.535767s
Real Time: 39.39453s
```

The first thing to note is that we have 210 PIDs here.  The parser defaults to showing only the top 25 PIDs in terms of active time, but you can change the number displayed with `--count / --c $NUMBER`, and how they are sorted with `--sort / -s $SORT_BY` (see `--help` for sorting options).

We see two processes that took 20 seconds, 19130 and 19128, let's start by looking at them.

```
$ strace-parser question3.txt pid 19130 19128

PID 19128
730 syscalls, active time: 20798.748ms, total time: 20798.748ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  write          	     154	 20775.463	  1294.944	   134.906	     0.007	EPIPE: 1
  open           	      40	     3.168	     0.134	     0.079	     0.014	ENOENT: 14
  mmap           	      23	     2.533	     0.434	     0.110	     0.071
  fstat          	      30	     2.363	     0.153	     0.079	     0.045
  read           	      29	     2.050	     0.146	     0.071	     0.021
  close          	      27	     2.018	     0.115	     0.075	     0.007
  access         	      17	     1.907	     0.562	     0.112	     0.069	ENOENT: 8
  fcntl          	     256	     1.826	     0.098	     0.007	     0.005	EBADF: 239
  stat           	      19	     1.457	     0.131	     0.077	     0.012	ENOENT: 12
  lstat          	      17	     1.328	     0.104	     0.078	     0.068	ENOENT: 14
  mprotect       	      14	     1.244	     0.128	     0.089	     0.075
  brk            	       9	     0.743	     0.139	     0.083	     0.045
  getdents       	      10	     0.694	     0.089	     0.069	     0.042
  rt_sigaction   	      65	     0.640	     0.103	     0.010	     0.006	EINVAL: 2
  getcwd         	       5	     0.364	     0.100	     0.073	     0.044
  execve         	       1	     0.242	     0.242	     0.242	     0.242
  rt_sigprocmask 	       3	     0.155	     0.076	     0.052	     0.006
  getrlimit      	       2	     0.141	     0.073	     0.071	     0.068
  munmap         	       1	     0.084	     0.084	     0.084	     0.084
  set_robust_list	       1	     0.076	     0.076	     0.076	     0.076
  arch_prctl     	       1	     0.075	     0.075	     0.075	     0.075
  set_tid_address	       1	     0.075	     0.075	     0.075	     0.075
  lseek          	       1	     0.069	     0.069	     0.069	     0.069
  dup2           	       3	     0.019	     0.007	     0.006	     0.006
  chdir          	       1	     0.013	     0.013	     0.013	     0.013
  ---------------

  Program Executed: /opt/gitlab/embedded/bin/git
  Args: ["rev-list" "4c245ea4810675ccd2e8b19a85236fe6f6f60b65" "--not" "--all" "--objects" "--in-commit-order"]

  Parent PID:  17487

  Slowest file open times for PID 19128:

   open (ms)	    timestamp    	        error     	   file name
  ----------	-----------------	   ---------------	   ---------
       0.134	 12:08:49.397719 	          -       	   /opt/gitlab/embedded/lib/libpcre2-8.so.0
       0.125	 12:08:49.396153 	       ENOENT     	   /opt/gitlab/embedded/lib/tls/x86_64/libpcre2-8.so.0
       0.125	 12:08:49.396793 	       ENOENT     	   /opt/gitlab/embedded/lib/tls/libpcre2-8.so.0
       0.112	 12:08:49.408065 	          -       	   /lib/x86_64-linux-gnu/libc.so.6
       0.103	 12:08:49.406308 	          -       	   /lib/x86_64-linux-gnu/librt.so.1
       0.100	 12:08:49.405900 	       ENOENT     	   /opt/gitlab/embedded/lib/librt.so.1
       0.098	 12:08:49.434606 	       ENOENT     	   shallow
       0.096	 12:08:49.397405 	       ENOENT     	   /opt/gitlab/embedded/lib/x86_64/libpcre2-8.so.0
       0.095	 12:08:49.432039 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/pack-088129d5f1630fac3172d3c92c5d6567a13716e1.idx
       0.094	 12:08:49.407704 	       ENOENT     	   /opt/gitlab/embedded/lib/libc.so.6


PID 19130
12 syscalls, active time: 20840.652ms, total time: 20841.414ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  read           	       1	 20840.223	 20840.223	 20840.223	 20840.223	ERESTARTSYS: 1
  futex          	       3	     0.761	     0.448	     0.254	     0.007
  rt_sigreturn   	       1	     0.213	     0.213	     0.213	     0.213	EINTR: 1
  madvise        	       1	     0.158	     0.158	     0.158	     0.158
  write          	       1	     0.023	     0.023	     0.023	     0.023
  prctl          	       1	     0.008	     0.008	     0.008	     0.008
  sigaltstack    	       1	     0.007	     0.007	     0.007	     0.007
  fstat          	       1	     0.007	     0.007	     0.007	     0.007
  set_robust_list	       1	     0.007	     0.007	     0.007	     0.007
  sched_getaffinity	       1	     0.006	     0.006	     0.006	     0.006
  exit           	       0	     0.000	     0.000	     0.000	     0.000
  ---------------

  Parent PID:  17487
```

We can see a few things of interest here:

   * 19128 spent almost all of its time on `write`
      * The slowest write was over a second, but the fastest was only seven microseconds - we should check this
   * 19128 is a performing a `git rev-list`
   * 19130 spent almost all its time on `read`
      * Just a single slow call
   * Both of these are children of PID 17487

Let's check `write` on 19128:

```
$ strace-parser question3.txt quantize write -p 19128

  syscall: write
  pids: 19128

    μsecs       	     count	 distribution
    ------------	  --------	 ----------------------------------------
       4 -> 7   	         7	|▇▇▇▇                                    |
       8 -> 15  	        18	|▇▇▇▇▇▇▇▇▇▇                              |
      16 -> 31  	         4	|▇▇                                      |
      32 -> 63  	        32	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇                      |
      64 -> 127 	        68	|▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇|
     128 -> 255 	         3	|▇                                       |
     256 -> 511 	         0	|                                        |
     512 -> 1K  	         0	|                                        |
      1K -> 2K  	         0	|                                        |
      2K -> 4K  	         0	|                                        |
      4K -> 8K  	         0	|                                        |
      8K -> 16K 	         0	|                                        |
     16K -> 32K 	         1	|                                        |
     32K -> 64K 	         0	|                                        |
     64K -> 128K	         0	|                                        |
    128K -> 256K	         0	|                                        |
    256K -> 512K	         1	|                                        |
    512K -> 1M  	         9	|▇▇▇▇▇                                   |
      1M -> 2M  	        11	|▇▇▇▇▇▇                                  |
```

So we have a split, most of the calls have a typical duration, but 20 are between 500 and 2000 milliseconds.  Let's grep for `write`s that took over a second:

```
$ grep '^19128.*write.*<' question3.txt
19128 12:08:52.202516 <... write resumed> ) = 4096 <1.232696>
19128 12:08:53.256811 <... write resumed> ) = 4096 <1.053889>
...
```

Hmm, because `strace` splits long calls over two lines, we can't see where this write was going.

We can get the context from `grep`, but because the events from each PID are mixed together there will be a lot of noise.

To get around this we'll first grep to get just the events from 19128, then get the context:

```
$ grep '^19128' question3.txt | grep -B1 '^19128.*write.*<[1-9]'
19128 12:08:50.969795 write(1<pipe:[86805]>, "90cb42f370bda9e7f4f58d9b8b8ee2750c115f vendor/github.com/sirupsen/logrus/LICENSE\n072e99be3137632f64c1bf57c49a67a199f74026 vendor/github.com/sirupsen/logrus/README.md\n8af90637a99e61a5f55df746e1f916b4b0ffab06 vendor/github.com/sirupsen/logrus/alt_exit.go\n96c2ce15f842b92c87952ba5702d33c37ccf1344 vendor/github.com/sirupsen/logrus/appveyor.yml\nda67aba06debf6721a2d97604145aa09ac257e19 vendor/github.com/sirupsen/logrus/doc.go\nca634a60974adc1ef815ef7a5fdc427dc8803b57 vendor/github.com/sirupsen/logrus/entry.go\nfb2a7a1f07040ac5905a8f61c4c07f5cbf21874f vendor/github.com/sirupsen/logrus/exported.go\nbe2f3fcee88c9a33b05f73cf80ebc5c1e335902a vendor/github.com/sirupsen/logrus/formatter.go\nf4fed02fb8ac563305634c2672890247e78e2f24 vendor/github.com/sirupsen/logrus/go.mod\n1f0d71964c2cb99378254f249e1373d76a7802bd vendor/github.com/sirupsen/logrus/go.sum\n3f151cdc39275a003d6a6e7059b0801071c001a8 vendor/github.com/sirupsen/logrus/hooks.go\nef8d0746084f9e4e675c963cf7b5371f25165262 vendor/github.com/sirupsen/logrus/json_formatter.go\nb"..., 4096 <unfinished ...>
19128 12:08:52.202516 <... write resumed> ) = 4096 <1.232696>
...
19128 12:09:08.786323 write(1<pipe:[86805]>, "db823a341a27d67c vendor/golang.org/x/sys/unix/zsysnum_linux_sparc64.go\nf60d8f988230b34128c371d25f6985664208f664 vendor/golang.org/x/sys/unix/zsysnum_netbsd_386.go\n48a91d46464d5a68937875a24bec4ae9693eaf56 vendor/golang.org/x/sys/unix/zsysnum_netbsd_amd64.go\n612ba662cb26a32b443adf27962ce848296f860f vendor/golang.org/x/sys/unix/zsysnum_netbsd_arm.go\n3e8ce2a1ddffd213a530aaf4dfe256cf8bbd9f9c vendor/golang.org/x/sys/unix/zsysnum_openbsd_386.go\nbd28146ddd5ee83d33f653954012d602e4c2ab92 vendor/golang.org/x/sys/unix/zsysnum_openbsd_amd64.go\nc708659859020fdd5907c5a4b7ffc5febd8abd44 vendor/golang.org/x/sys/unix/zsysnum_solaris_amd64.go\n2de1d44e281dd1b8b3822d6c7eac070d0e3e01fc vendor/golang.org/x/sys/unix/ztypes_darwin_386.go\n044657878c854f5c98d12b6e88364370667f8a7d vendor/golang.org/x/sys/unix/ztypes_darwin_amd64.go\n66df363ce5be96d32a84be7b75178a6ebf33748c vendor/golang.org/x/sys/unix/ztypes_darwin_arm.go\n85d56eabd3fa4c1167ea86afdad3d125d90876db vendor/golang.org/x/sys/unix/ztypes_darwin_arm64.go\ne585c893abcd2fbd97fdb354"..., 4096 <unfinished ...>
19128 12:09:09.849160 <... write resumed> ) = 4096 <1.062823>
```

So 19128 is piping the output of `git rev-list` to another process.  Let's search for reads to this pipe from around this time:

```
$ grep '12:0[7-9].*read.*86805' question3.txt
17487 12:08:49.400501 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:49.440924 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:50.134963 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:50.969212 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:52.202425 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:53.256722 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:54.359725 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:55.478193 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:56.773472 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:57.889677 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:58.778214 read(13<pipe:[86805]>,  <unfinished ...>
17487 12:08:59.532873 read(13<pipe:[86805]>, "86d596f74a619efba25ff047b6d9123f7b1 vendor/github.com/prometheus/client_golang/prometheus/metric.go\n5806cd09e306cdd9c73df71839fc1273a5e2fc3f vendor/github.com/prometheus/client_golang/prometheus/observer.go\n55176d58ce6a213fe4008f6b2f8630709bd031cc vendor/github.com/prometheus/client_golang/prometheus/process_collector.go\n3bd2cc471c7f173a0f3e842f58a574ab2adb95fa vendor/github.com/prometheus/client_golang/prometheus/promhttp\n67b56d37cfd726f0fb7b3c75013d4e4e5e411584 vendor/github.com/prometheus/client_golang/prometheus/promhttp/delegator.go\n31a70695695c59b58609554743f13ff8a8e89cdd vendor/github.com/prometheus/client_golang/prometheus/promhttp/delegator_1_8.go\n8bb9b8b68f8b4a721d75b2d6e3e4f22af9088489 vendor/github.com/prometheus/client_golang/prometheus/promhttp/delegator_pre_1_8.go\n01357374feb1699d6d3c43d5d880fa9cfc8a42ee vendor/github.com/prometheus/client_golang/prometheus/promhttp/http.go\n86fd564470f8161f9696beab0bb51d00c69d1c1b vendor/github.com/prometheus/client_golang/prometheus/promhttp/instrument_client."..., 8192) = 8192 <0.000032>
...
```

17487 is 19128's parent process, makes sense.  What else is 17487 doing at this time?

```
$ grep '^17487.*12:08' question3.txt
...
7487 12:08:49.444710 access("/var/opt/gitlab/.gitconfig", F_OK <unfinished ...>
17487 12:08:49.444791 <... access resumed> ) = 0 <0.000073>
17487 12:08:49.444846 stat("/var/opt/gitlab/.gitconfig",  <unfinished ...>
17487 12:08:49.444921 <... stat resumed> {st_mode=S_IFREG|0644, st_size=292, ...}) = 0 <0.000065>
17487 12:08:49.444990 open("/var/opt/gitlab/.gitconfig", O_RDONLY|O_CLOEXEC) = 11</var/opt/gitlab/.gitconfig> <0.000048>
17487 12:08:49.445141 read(11</var/opt/gitlab/.gitconfig>,  <unfinished ...>
17487 12:08:49.445184 <... read resumed> "# This file is managed by gitlab-ctl. Manual changes will be\n# erased! To change the contents below, edit /etc/gitlab/gitlab.rb\n# and run `sudo gitlab-ctl reconfigure`.\n\n[user]\n        name = GitLab\n        email = gitlab@192.168.122.160\n[core]\n        autocrlf = input\n[gc]\n        auto = 0\n", 292) = 292 <0.000031>
17487 12:08:49.445227 close(11</var/opt/gitlab/.gitconfig>) = 0 <0.000070>
17487 12:08:49.445394 stat("/var/opt/gitlab/git-data/repositories/root/blah.git/config", {st_mode=S_IFREG|0644, st_size=97, ...}) = 0 <0.000071>
17487 12:08:49.445550 open("/var/opt/gitlab/git-data/repositories/root/blah.git/config", O_RDONLY|O_CLOEXEC) = 11</var/opt/gitlab/git-data/repositories/root/blah.git/config> <0.000092>
17487 12:08:49.445733 read(11</var/opt/gitlab/git-data/repositories/root/blah.git/config>, "[core]\n\tbare = true\n\trepositoryformatversion = 0\n\tfilemode = true\n[gitlab]\n\tfullpath = root/blah\n", 97) = 97 <0.000069>
17487 12:08:49.445889 close(11</var/opt/gitlab/git-data/repositories/root/blah.git/config>) = 0 <0.000068>
17487 12:08:49.446044 stat("/var/opt/gitlab/.gitconfig", {st_mode=S_IFREG|0644, st_size=292, ...}) = 0 <0.000069>
17487 12:08:49.446200 open("/var/opt/gitlab/.gitconfig", O_RDONLY|O_CLOEXEC) = 11</var/opt/gitlab/.gitconfig> <0.000069>
17487 12:08:49.446358 mprotect(0x7f94ac025000, 4096, PROT_READ|PROT_WRITE) = 0 <0.000067>
17487 12:08:49.446509 read(11</var/opt/gitlab/.gitconfig>, "# This file is managed by gitlab-ctl. Manual changes will be\n# erased! To change the contents below, edit /etc/gitlab/gitlab.rb\n# and run `sudo gitlab-ctl reconfigure`.\n\n[user]\n        name = GitLab\n        email = gitlab@192.168.122.160\n[core]\n        autocrlf = input\n[gc]\n        auto = 0\n", 292) = 292 <0.000068>
17487 12:08:49.446665 close(11</var/opt/gitlab/.gitconfig>) = 0 <0.000068>
17487 12:08:49.446841 stat("/var/opt/gitlab/git-data/repositories/root/blah.git/config", {st_mode=S_IFREG|0644, st_size=97, ...}) = 0 <0.000072>
17487 12:08:49.447001 open("/var/opt/gitlab/git-data/repositories/root/blah.git/config", O_RDONLY|O_CLOEXEC) = 11</var/opt/gitlab/git-data/repositories/root/blah.git/config> <0.000072>
17487 12:08:49.447164 read(11</var/opt/gitlab/git-data/repositories/root/blah.git/config>, "[core]\n\tbare = true\n\trepositoryformatversion = 0\n\tfilemode = true\n[gitlab]\n\tfullpath = root/blah\n", 97) = 97 <0.000070>
...
```

There's a whole bunch of noise in this, but there's a pattern of 17487 repeatedly opening, reading, and closing the `.gitconfig` files for the instance and project over and over again.

</details>

***

   * Do any syscalls have an unusually high call count?


<details>
<summary>Answer</summary>

We saw in the last section that 17487 is acting strangely with `.gitconfig`.  Let's look at this PID in more detail.

```
$ strace-parser question3.txt pid 17487

PID 17487
132729 syscalls, active time: 9141.285ms, total time: 20692.807ms

  syscall        	   count	total (ms)	  max (ms)	  avg (ms)	  min (ms)	errors
  ---------------	--------	----------	----------	----------	----------	--------
  futex          	     221	 11551.521	 11531.542	    52.269	     0.006	EAGAIN: 50
  stat           	   44463	  3023.542	     4.839	     0.068	     0.007	ENOENT: 6063
  open           	   16170	  1123.439	     4.820	     0.069	     0.008
  close          	   16185	  1095.836	     5.126	     0.068	     0.007
  access         	   14148	   967.917	     4.149	     0.068	     0.007	ENOENT: 8084
  lstat          	   14147	   957.741	     2.067	     0.068	     0.007
  read           	   12155	   858.832	    40.326	     0.071	     0.007
  getdents       	    8084	   566.559	    17.545	     0.070	     0.007
  fstat          	    4045	   264.524	     1.047	     0.065	     0.006
  mprotect       	    2975	   201.345	     2.670	     0.068	     0.007
  vfork          	       2	    70.724	    62.039	    35.362	     8.685
  clone          	       4	     2.104	     0.666	     0.526	     0.410
  write          	      26	     1.735	     0.293	     0.067	     0.013
  sched_yield    	      40	     1.618	     0.084	     0.040	     0.006
  fcntl          	      12	     1.298	     0.205	     0.108	     0.067
  ioctl          	      12	     1.178	     0.140	     0.098	     0.067	ENOTTY: 12
  pipe2          	       8	     0.851	     0.156	     0.106	     0.068
  mmap           	       6	     0.635	     0.161	     0.106	     0.073
  clock_gettime  	      13	     0.531	     0.168	     0.041	     0.006
  rt_sigprocmask 	       4	     0.281	     0.119	     0.070	     0.019
  getcwd         	       1	     0.166	     0.166	     0.166	     0.166
  getresgid      	       2	     0.156	     0.081	     0.078	     0.075
  getresuid      	       2	     0.140	     0.074	     0.070	     0.066
  lseek          	       1	     0.070	     0.070	     0.070	     0.070
  sendmsg        	       2	     0.045	     0.035	     0.023	     0.010
  tgkill         	       1	     0.019	     0.019	     0.019	     0.019
  ---------------

  Child PIDs:  19125, 19126, 19127, 19128, 19129, 19130

  Slowest file open times for PID 17487:

   open (ms)	    timestamp    	        error     	   file name
  ----------	-----------------	   ---------------	   ---------
       4.820	 12:09:04.998166 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
       1.752	 12:08:59.457781 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/pack/
       1.204	 12:08:50.396195 	          -       	   /var/opt/gitlab/.gitconfig
       1.149	 12:08:54.303314 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/
       1.069	 12:08:57.885565 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/pack/
       0.881	 12:08:58.707773 	          -       	   /var/opt/gitlab/.gitconfig
       0.822	 12:09:03.518392 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
       0.801	 12:08:50.304888 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
       0.789	 12:09:03.418048 	          -       	   /var/opt/gitlab/.gitconfig
       0.768	 12:09:05.924747 	          -       	   /var/opt/gitlab/.gitconfig
```

We can ignore `futex`, that's just the process waiting, most likely on the SSH transfer.  There are a lot of `open`, `close`, and `read` events which matches up with the snippet we grepped.

Also of interest is the amount of time spent on `read`. 19128 spent 20 seconds writing to the pipe, but 17487 is only spending 858ms on `read` calls, and most of those are probably reading the `.gitconfig` files.

Let's take a close look at the files opened

```
$ strace-parser question3.txt files -p 17487
    17487	       0.073	 12:08:49.444065 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
    17487	       0.048	 12:08:49.444990 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.092	 12:08:49.445550 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
    17487	       0.069	 12:08:49.446200 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.072	 12:08:49.447001 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
... many results
    17487	       0.008	 12:09:10.237396 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.016	 12:09:10.237563 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/pack/
    17487	       0.009	 12:09:10.237869 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/
    17487	       0.009	 12:09:10.238713 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
    17487	       0.008	 12:09:10.238883 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.009	 12:09:10.239000 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
    17487	       0.008	 12:09:10.239109 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.013	 12:09:10.239228 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/config
    17487	       0.009	 12:09:10.239431 	          -       	   /var/opt/gitlab/.gitconfig
    17487	       0.009	 12:09:10.239596 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/pack/
    17487	       0.009	 12:09:10.239894 	          -       	   /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/
```

Looks like the same files being repeated over the course of 20 seconds, let's get a count:

```
$ strace-parser question3.txt files -p 17487 | awk '{print $5}' | sort | uniq -c | sort -n
/var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/pack-088129d5f1630fac3172d3c92c5d6567a13716e1.idx
   1 /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/pack-088129d5f1630fac3172d3c92c5d6567a13716e1.pack
   1 error
   4
2021 /var/opt/gitlab/git-data/repositories/root/blah.git/objects/incoming-0MbK9e/pack/
2021 /var/opt/gitlab/git-data/repositories/root/blah.git/objects/pack/
6063 /var/opt/gitlab/.gitconfig
6063 /var/opt/gitlab/git-data/repositories/root/blah.git/config
```

The `.gitconfig` files are opened 6000 times, and the `pack` 2000 times.  This seems unnecessary for an action that touches a single project.

</details>

***

   * What is causing these syscalls to take so long?

<details>
<summary>Answer</summary>

Here's the scenario we see so far:

> PID 17487 forks a `git rev-list` process, PID 19128
> PID 19128 will attempt to pipe back a list of refs to its parent
> PID 17487 is going into a loop of reading `.gitconfig` files 6000 times after forking
> PID 19128's `write` calls to 17487 are going unanswered for about a second each time

Going back to the error logged by Gitaly, it looks like 17487 is probably GetNewLFSPointers method

```
2018-12-05_18:30:53.13130 time="2018-12-05T18:30:53Z" level=info msg="finished streaming call" error="rpc error: code = Canceled desc = rpc error: code = Canceled desc = context canceled" grpc.code=Canceled grpc.meta.auth_version=v1 grpc.meta.client_name=gitlab-web grpc.method=GetNewLFSPointers grpc.request.glRepository=project-1 grpc.request.repoPath=group/projects.git grpc.request.repoStorage=default grpc.request.topLevelGroup=core-build grpc.service=gitaly.BlobService grpc.time_ms=30000 peer.address=@ span.kind=server system=grpc
```

First let's look at [`GetNewLFSPointers`](https://gitlab.com/gitlab-org/gitaly/blob/v0.125.1/ruby/lib/gitaly_server/blob_service.rb#L22-35)
```rb
    def get_new_lfs_pointers(request, call)
      Enumerator.new do |y|
        bridge_exceptions do
          changes = lfs_changes(request, call)
          object_limit = request.limit.zero? ? nil : request.limit
          not_in = request.not_in_all ? :all : request.not_in_refs.to_a
          blobs = changes.new_pointers(object_limit: object_limit, not_in: not_in)

          sliced_gitaly_lfs_pointers(blobs) do |lfs_pointers|
            y.yield Gitaly::GetNewLFSPointersResponse.new(lfs_pointers: lfs_pointers)
          end
        end
      end
    end
```

A quick look shows the work is being done in [`changes.new_pointers(...)`](https://gitlab.com/gitlab-org/gitaly/blob/v0.125.1/ruby/lib/gitlab/git/lfs_changes.rb#L9-25)
```rb
      def new_pointers(object_limit: nil, not_in: nil)
        git_new_pointers(object_limit, not_in)
      end

      def all_pointers
        git_all_pointers
      end

      private

      def git_new_pointers(object_limit, not_in)
        rev_list.new_objects(rev_list_params(not_in: not_in)) do |object_ids|
          object_ids = object_ids.take(object_limit) if object_limit

          Gitlab::Git::Blob.batch_lfs_pointers(@repository, object_ids)
        end
      end
```

So `git_new_pointer` calls `rev_list` and then takes the results and passes it to [`batch_lfs_pointers`](https://gitlab.com/gitlab-org/gitaly/blob/v0.125.1/ruby/lib/gitlab/git/blob.rb#L61-67).  This corresponds with 19128 piping its results to 17487. 

```rb
        # Find LFS blobs given an array of sha ids
        # Returns array of Gitlab::Git::Blob
        # Does not guarantee blob data will be set
        def batch_lfs_pointers(repository, blob_ids)
          blob_ids.lazy
                  .select { |sha| possible_lfs_blob?(repository, sha) }
                  .map { |sha| rugged_raw(repository, sha, limit: LFS_POINTER_MAX_SIZE) }
                  .select(&:lfs_pointer?)
                  .force
        end
```
The `lazy` modifier explains the long writes we saw for 19128.  The method will only consume output from `rev_list` as needed, so it's taking 17487 about a second to fully consume each batch of refs that was piped to to.

We see the Gitaly timeouts when there are so many refs that `git rev-list` take over 30 seconds Gitaly.

So what's causing 17487 to spend so much time reading the `.gitconfig` files for a project over and over again?  Let's look at [`possible_lfs_blob`](https://gitlab.com/gitlab-org/gitaly/blob/v0.125.1/ruby/lib/gitlab/git/blob.rb#L61-67) first:

```rb
        # Efficient lookup to determine if object size
        # and type make it a possible LFS blob without loading
        # blob content into memory with repository.lookup(sha)
        def possible_lfs_blob?(repository, sha)
          object_header = repository.rugged.read_header(sha)

          object_header[:type] == :blob &&
            size_could_be_lfs?(object_header[:len])
        end
      end
```

We get the object header then check it's size, OK.  How do we get the header in [`repository.rugged`](https://gitlab.com/gitlab-org/gitaly/blob/v0.125.1/ruby/lib/gitlab/git/repository.rb#L167-171)?

```rb
      def rugged
        Rugged::Repository.new(path, alternates: alternate_object_directories)
      rescue Rugged::RepositoryError, Rugged::OSError
        raise NoRepository.new('no repository for such path')
      end
```

We don't check if an existing `Repository` object exists.  Instead we create a new Rugged::Repository object for each SHA we check, and each one of those needs to read `.gitconfig` upon creation.

Why did the problem stop with [11.5.0](https://gitlab.com/gitlab-org/gitaly/blob/v0.129.0/ruby/lib/gitlab/git/repository.rb#L164-172)?

```rb
      def rugged
        @rugged ||= begin
                      Rugged::Repository.new(path, alternates: alternate_object_directories).tap do |repo|
                        Thread.current[RUGGED_KEY] << repo if Thread.current[RUGGED_KEY]
                      end
                    end
      rescue Rugged::RepositoryError, Rugged::OSError
        raise NoRepository, 'no repository for such path'
      end
```

The method was changed to only create one new Repository object, much better!

This was a long-winded summary of https://gitlab.com/gitlab-org/gitaly/issues/1428.

</details>
